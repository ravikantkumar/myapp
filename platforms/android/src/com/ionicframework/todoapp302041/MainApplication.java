package com.ionicframework.todoapp302041; //replace with your package name

import android.app.Application;
import org.apache.cordova.*;
import com.parse.Parse;
import com.parse.ParseAnalytics;
import com.parse.ParseInstallation;
import com.parse.PushService;
import com.parse.ParsePush;
import com.parse.ParseCrashReporting;
 
//// Track app opens.
//ParseAnalytics.trackAppOpened(getIntent());
// in cordova activty
public class MainApplication extends Application {
 
    @Override
    public void onCreate() {
      super.onCreate();
	
      ParseCrashReporting.enable(getApplicationContext()); //this only if you want to use crash reporting
      Parse.initialize(this, "F8TOhK8zoN69mY0OydaZBgVOcFT4xAxlLYegGFX2", "G1C9h3fBpsj1xdtuwNJmJODoWxTjxl9hapSblF72");
      PushService.setDefaultPushCallback(this, MainActivity.class);
      ParsePush.subscribeInBackground("Broadcast"); // if you want all app users to subscribe to a channel
      ParseInstallation.getCurrentInstallation().saveInBackground();
    }
}
