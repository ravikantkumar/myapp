var app = angular.module('starter.controllers', ["angular-datepicker"]);
app.controller('AddTodoCtrl', function($http, $ionicPlatform,$cordovaLocalNotification, $ionicActionSheet, $timeout, $ionicHistory, $cordovaDatePicker, $cordovaContacts, $ionicModal, Alertuser, $window, $ionicLoading, $ionicPopup, $cordovaNetwork, $scope, $state, $rootScope, $q, ConnectParse, $localStorage) {
    $scope.isTodayVisible = true;
    $scope.isLaterVisible = true;
    $scope.isThisWeekVisible = true;
    $scope.isCompletedVisible = true;
    $scope.listCanSwipe = true;


    if ($localStorage['loggedUsername']) {
        $scope.loggedUser = $localStorage['loggedUsername'];
    } else {
        $scope.loggedUser = "";
    }

    $scope.add = {
        contactSearch: ''
    };


        var searchBack=$ionicPlatform.registerBackButtonAction(
                        function (e) {
                            e.preventDefault();
                            
                     console.log("close the popup");
                     $scope.hideSearch();
                 }, 1000
            );
            $scope.$on('$destroy', searchBack);
            
    $scope.showHeaderSearch = function() {
        $scope.showSearch = true;
        $scope.hideHeader = true;
        $scope.searchFocus = true;
        $scope.hideCreateTodo=true;
    };
    $scope.hideSearch = function() {
        $scope.searchTask = null;
        $scope.showSearch = false;
        $scope.hideHeader = false;
        $scope.searchFocus = false;
        $scope.hideCreateTodo=false;
    };

    $scope.blurfun = function(task) {
        // var task=$scope.mytask.title;
        $scope.inFo = " ";
        $scope.blFo = " ";
        if (!task) {
            console.log("out Of Focus");
            $scope.inFo = false;
            $scope.blFo = true;
        } else {
            console.log("In Focus");
            $scope.inFo = true;
            $scope.blFo = false;
        }
    };
    // Whatsapp Sharing...
    $scope.shareViaWhatsapp = function() {
        window.plugins.socialsharing.available(function(isAvailable) {
            if (isAvailable) {
                 initUrl();
                console.log("received" + JSON.stringify(sharedTodo));
                console.log("enodedObject" + encodeURIComponent(sharedTodo));
                var todoString = JSON.stringify(sharedTodo);
                var encodeTodo = encodeURIComponent(todoString);
                var todoUrlW = 'http://excellencetechnologies.co.in/ravikant/todoApp/App.php?task=' + encodeTodo + '';
                console.log(todoUrlW);
                console.log(decodeURIComponent(todoString));  
                //FUNCTION TO MAKE URL SHORT
                function makeRequestWhatsapp() {
                   var request = gapi.client.urlshortener.url.insert({
                     'resource': {
                           'longUrl': todoUrlW
                         }
                   });
                   request.then(function(response) {
                     console.log(response.result.id);
                      $localStorage['wShortUrl']=" ";
                      $localStorage['wShortUrl']=response.result.id;
                      console.log($localStorage['wShortUrl']);
                   }, function(reason) {
                     console.log('Error: ' + reason.result.error.message);
                   });
                 }
                function initUrl() {
                   gapi.client.setApiKey('AIzaSyBP0C4zOlMFFEOWZnK9oGIkUITACR0vNXg');
                   gapi.client.load('urlshortener', 'v1').then(makeRequestWhatsapp);
                 }
                
                window.plugins.socialsharing.shareViaWhatsApp("Shared TodoApp link", null, $localStorage['wShortUrl'], function() {
                 Alertuser.saveAlert("shared...");
                }, function(e) {
                    alert("error: " + e);
                });

            }
        });
    };

    $scope.showReorder = false;
    $rootScope.ifdeviceReady = true;
    //if (window.cordova && !$cordovaNetwork.isOnline()) {
    //    $state.go('offline');
    //}
    $scope.moredata = true;
    $scope.todoList = [];
    $scope.todoAlarm;
    var skip = 10;
    var todostatus = {};
    todostatus.true = "item-completed";
    todostatus.false = "item-not-completed";
    // colour picker
    var color = ["#FFE6E6", "#FFB2B2", "#FFFFF0", "#F5E0EB", "#FAF0F5", "#E6FAF0", "#CCF5E0", "#E6F5FA", "#B2E0F0", "#FFE0B2", "#FFF5E6", "#FFF0FF", "#FFE6FF", "#E0EBEB", "#D1E0E0", ];
    $scope.addSelf = function(num) {
        var sum = 0;
        while (num > 0)
        {
            sum += num % 10;
            num = Math.floor(num / 10);
        }
        if (sum > 9)
        {
            sum = $scope.addSelf(sum);
        }
        return sum;
    };
    $scope.colorPicker = function(value) {
        var colorVal = 0;
        var alphabets = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"];
        for (var i = 0; i < value.length; i++) {
            var char = value.substr(i, 1);
            var charValue = alphabets.indexOf(char);
            colorVal = colorVal + charValue;
        }
        //multiply by tag 3 alphabet value, - last alphabet
        var x = (colorVal * alphabets.indexOf(value.substr(2, 1))) - (alphabets.indexOf(value.substr((value.length - 1), 1)));
        if (x > 20) {
            x = $scope.addSelf(colorVal);
        }
        //multiply by tag 3 alphabet value
        return color[x];
    };
    //transfer the localstorage todo's to $scope.todolist(array)

    $scope.processLocalStorage = function(xTodoList) {
        var result = xTodoList;
        var mytodolist = [];
        if (result && result.length > 0) {

            for (var i = 0; i < result.length; i++) {
                var mytodoobj = {};
                if(result[i].Unique_Id){
                    mytodoobj.taskId=result[i].Unique_Id;
                }
                mytodoobj.time = result[i].time;
                mytodoobj.alarmTime = result[i].alarmTime;
                if (!result[i].parseStatus === false) {
                    mytodoobj.parseStatus = false;
                }
                else {
                    mytodoobj.parseStatus = result[i].parseStatus;
                }
                mytodoobj.title = result[i].title;
                mytodoobj.done = result[i].done;
                mytodoobj.position = result[i].position;
                mytodoobj.completed = todostatus[mytodoobj.done];
                mytodolist.unshift(mytodoobj);
            }

            $scope.todoList = mytodolist;
        }
    };
    $scope.processDeletedLocalStorage = function(deletedTodoList) {
        var result = deletedTodoList;
        var mytodolist = [];
        if (result && result.length > 0) {

            for (var i = 0; i < result.length; i++) {
                var mytodoobj = {};
                if(result[i].Unique_Id){
                    mytodoobj.taskId=result[i].Unique_Id;
                }
                mytodoobj.time = result[i].time;
                mytodoobj.alarmTime = result[i].alarmTime;
                if (!result[i].parseStatus === false) {
                    mytodoobj.parseStatus = false;
                }
                else {
                    mytodoobj.parseStatus = result[i].parseStatus;
                }
                mytodoobj.title = result[i].title;
                //mytodoobj.done = result[i].done;
               // mytodoobj.position = result[i].position;
               // mytodoobj.completed = todostatus[mytodoobj.done];
                mytodolist.unshift(mytodoobj);
            }

            $scope.deletedtodoList = mytodolist;
            console.log($scope.deletedtodoList);
        }
    };
    // things to do before enter to ionic view...
    $scope.$on('$ionicView.beforeEnter', function() {
        if ($localStorage['loggedUsername']) {
            $scope.loggedUser = $localStorage['loggedUsername'];
        } else {
            $scope.loggedUser = "";
        }
        if ($localStorage["Initializer"]) {
            console.log("initialize..");
            $scope.msg = "Todo Tasks";
            var checkTodoList = $q.when(ConnectParse.fetchRecord(parseInt($localStorage["Initializer"])), 0);
            checkTodoList.then(
                    function(result) {
                        var mytodolist = [];
                        if (result.length > 0) {
                            for (var i = 0; i < result.length; i++) {
                                console.log(result[i]);
                                var mytodoobj = {};
                                console.log(result[i].id);
                                mytodoobj.Unique_Id = result[i].id;
                                mytodoobj.title = result[i].get("todo_title");
                                mytodoobj.time = result[i].get("time");
                                mytodoobj.alarmTime = result[i].get("alarmTime");
                                mytodoobj.parseStatus = result[i].get("parseStatus");
                                mytodoobj.done = result[i].get("done");
                                mytodoobj.position = result[i].get("position");
                                mytodoobj.completed = todostatus[mytodoobj.done];
                                mytodolist.push(mytodoobj);
                            }
                            //$ionicLoading.hide();
                            $localStorage['todoTasks'] = mytodolist;
                            $scope.processLocalStorage($localStorage['todoTasks']);
                        }
                        else {
                            $scope.moredata = false;
                            console.log("No records found");
                            $localStorage['todoTasks'] = [];
                            $ionicLoading.hide();
                        }
                    },
                    function(error) {
                        console.log(error);
                    }
            );
                $scope.fetchDeletedTask($localStorage["Initializer"]);
        }
        else if (!$localStorage["Initializer"] && $localStorage['todoTasks']) {

            $scope.processLocalStorage($localStorage['todoTasks']);
        } else if (!$localStorage['todoTasks']) {
            $scope.msg = "Add new Todo Tasks!";
        }
    }); // end of before view change
    
    $scope.fetchDeletedTask=function(userid){
        var checkTodoList = $q.when(ConnectParse.fetchDeletedRecord(parseInt(userid)), 0);
            checkTodoList.then(
                    function(result) {
                        var deletedlist = [];
                        if (result.length > 0) {
                            for (var i = 0; i < result.length; i++) {
                                console.log(result[i]);
                                var deletedobj = {};
                                console.log(result[i].id);
                                deletedobj.Unique_Id = result[i].id;
                                deletedobj.title = result[i].get("todoTitle");
                                deletedobj.time = result[i].get("time");
                                deletedobj.alarmTime = result[i].get("alarmTime");
                                deletedobj.parseStatus = result[i].get("parseStatus");
                                //deletedobj.done = result[i].get("done");
                                //mytodoobj.position = result[i].get("position");
                                //deletedobj.completed = todostatus[deletedobj.done];
                                deletedlist.push(deletedobj);
                            }
                            //$ionicLoading.hide();
                            $localStorage['deletedTasks'] = deletedlist;
                            $scope.processDeletedLocalStorage($localStorage['deletedTasks']);
                        }
                        else {
                            $scope.moredata = false;
                            console.log("No records found");
                            $localStorage['deletedTasks'] = [];
                            //$ionicLoading.hide();
                        }
                    },
                    function(error) {
                        console.log(error);
                    }
            );
    };
   
    // Complete the Tasks..
    $scope.completedRecord = function(item, index) {
        if ($localStorage["Initializer"]) {                 //true when user is logged In and on desktop
            item.completed = todostatus[item.done];
            var searchRes = $scope.search(item.position, $scope.todoList);
            if (!searchRes) {
                console.log("position not found");
            }
            else {
                $scope.todoList.splice(index, 1);
                $scope.todoList.splice(index, 0, item);
            }
            var checkTodoList = $q.when(ConnectParse.checkIfRecordExist(parseInt($localStorage["Initializer"])));
            checkTodoList.then(
                    function(result) {
                        var newIndex;
                        if (result.length > 0) {
                            for (var i = 0; i < result.length; i++) {
                                if (item.position === result[i].get("position")) {
                                    newIndex = i;
                                    console.log(newIndex);
                                    var editRecord = $q.when(ConnectParse.complete(result[i], "done", item.done));
                                    editRecord.then(
                                            function(result) {
                                                console.log("completed");
                                            }, function(error) {
                                        console.log("Not completed");
                                    });
                                }
                            }
                        }
                        else {
                            console.log("No records found");
                        }
                    },
                    function(error) {
                        console.log(error);
                    }
            );
        }
        else {
            if (!$localStorage["Initializer"] && window.cordova) {  // true when open in Mobile



                if (item.done == true) {
                    item.done = true;
                    var tags = [];
                    tags = $localStorage['todoTasks'];
                    for (var i = 0; i < tags.length; i++) {
                        if (tags[i].title == item.title) {
                            tags[i].done = true;
                            tags[i].completed = todostatus.true;
                            break;
                        }
                    }
                    $localStorage['todoTasks'];
                }
                else {
                    item.done = false;
                    var tags = [];
                    tags = $localStorage['todoTasks'];
                    for (var i = 0; i < tags.length; i++) {
                        if (tags[i].title == item.title) {
                            tags[i].done = false;
                            tags[i].completed = todostatus.false;
                            break;
                        }
                    }
                    $localStorage['todoTasks']
                }
                item.completed = todostatus[item.done];
                var searchRes = $scope.search(item.position, $scope.todoList);
                if (!searchRes) {
                    console.log("position not found");
                }
                else {
                    $scope.todoList.splice(index, 1);
                    $scope.todoList.splice(index, 0, item);
                }
            }
            else {      // // true when open in Mobile and user is logged in

                if (item.done === true) {
                    item.done = true;
                    var tags = [];
                    tags = $localStorage["todoTasks"];
                    for (var i = 0; i < tags.length; i++) {
                        if (tags[i].title == item.title) {
                            tags[i].done = true;
                            tags[i].completed = todostatus.true;
                            break;
                        }
                    }
                    $localStorage["todoTasks"] = tags;
                }
                else {
                    item.done = false;
                    var tags = [];
                    tags = $localStorage["todoTasks"];
                    for (var i = 0; i < tags.length; i++) {
                        if (tags[i].title == item.title) {
                            tags[i].done = false;
                            tags[i].completed = todostatus.false;
                            break;
                        }
                        $localStorage["todoTasks"] = tags;
                    }
                }

                //return false;
            }
        }

    };
    
    $scope.reorderMe = function(item, from, to) {
        //refresh the display list
        var status;
        console.log(from + "  " + to + "  " + item.position);
        console.log(item);
        $scope.todoList.splice(from, 1);
        $scope.todoList.splice(to, 0, item);
        var newFrom = ($scope.todoList.length - from) - 1;
        var newTo = ($scope.todoList.length - to) - 1;
        if (newFrom < newTo)
            status = true;
        else
            status = false;
        console.log(status);
        var checkTodoList = $q.when(ConnectParse.checkPosition(parseInt($localStorage["Initializer"])));
        checkTodoList.then(
                function(result) {
                    //var temp_result = result;
                    if (status) {
                        console.log("down");
                        for (var i = 0; i < result.length; i++) {
                            if (result[i].get("position") >= newFrom && result[i].get("position") <= newTo) {
                                console.log(result[i].get("todo_title"));
                                if (result[i].get("position") == item.position) {
                                    console.log(result[i].get("todo_title"));
                                    console.log(result[i].get("position") + "--" + item.position);
                                    //set position of item to new position
                                    var editRecord = $q.when(ConnectParse.complete(result[i], "position", newTo));
                                    editRecord.then(
                                            function(result) {
                                                console.log("reposition set");
                                            }, function(error) {
                                        console.log("Not set");
                                    });
                                }
                                else {
                                    //increase the position by 1
                                    console.log(result[i].get("todo_title"));
                                    var editRecord = $q.when(ConnectParse.complete(result[i], "position", result[i].get("position") - 1));
                                    editRecord.then(
                                            function(result) {
                                                console.log("memposition set");
                                            }, function(error) {
                                        console.log("Not set");
                                    });
                                }
                            }
                        }
                        //reload
                        //$state.go($state.current, {}, {reload: true});
                    }
                    else {
                        for (var i = 0; i < result.length; i++) {
                            if (result[i].get("position") <= newFrom && result[i].get("position") >= newTo) {
                                if (result[i].get("position") == item.position) {
                                    console.log(result[i].get("position") + "--" + item.position);
                                    //set position of item to new position
                                    var editRecord = $q.when(ConnectParse.complete(result[i], "position", newTo));
                                    editRecord.then(
                                            function(result) {
                                                console.log("reposition set");
                                            }, function(error) {
                                        console.log("Not set");
                                    });
                                }
                                else {
                                    //increase the position by 1
                                    var editRecord = $q.when(ConnectParse.complete(result[i], "position", result[i].get("position") + 1));
                                    editRecord.then(
                                            function(result) {
                                                console.log("mem-position set");
                                            }, function(error) {
                                        console.log("Not set");
                                    });
                                }
                            }
                        }
                        //reload
                        //$state.go($state.current, {}, {reload: true});
                    }
                },
                function(error) {
                    console.log(error);
                }
        );
    };
    $scope.callEdit = function(index, item) {
        console.log(this.showReorder);
        if (this.showReorder) {
            console.log(this.showReorder);
            return false;
        }
        console.log("edit called");
        var time = item.time;
        $scope.data = {};
        // An elaborate, custom popup
//        $ionicPopup.prompt({
//            title: 'Edit you todo',
//            inputType: 'text',
//            inputPlaceholder: 'Enter your Todo here',
//            okText: 'SAVE',
//            okType: 'button-calm'
//        }).then(function(res) {
//            if (!res || res === " " || res.length === 0) {
//                return false;
//            }
//            var editItem = {
//                time: new Date().getTime(),
//                title: res,
//                parseStatus: false,
//                position: item.position,
//                done: false
//            };
//            $scope.todoList.splice(index, 1);
//            $scope.todoList.splice(index, 0, editItem);
//            $scope.editRecord(time, editItem);
//        });
    };
//            $scope.editRecord = function(time, editTodo) {
//                console.log(time + " of " + editTodo);
//                if ($localStorage["Initializer"]) {
//                    var checkTodoList = $q.when(ConnectParse.checkIfRecordExist(parseInt($localStorage["Initializer"])));
//                    checkTodoList.then(
//                            function(result) {
//                                var newIndex;
//                                if (result.length > 0) {
//                                    for (var i = 0; i < result.length; i++) {
//                                        if (time === result[i].get("time")) {
//                                            newIndex = i;
//                                            console.log(newIndex);
//                                            var editRecord = $q.when(ConnectParse.update(result[i], "todo_title", editTodo));
//                                            editRecord.then(
//                                                    function(result) {
//                                                        console.log("edited");
//                                                    }, function(error) {
//                                                console.log("Not edited");
//                                            });
//                                        }
//                                    }
//                                }
//                                else {
//                                    console.log("No records found");
//                                }
//                            },
//                            function(error) {
//                                console.log(error);
//                            }
//                    );
//                } else {
//                    var newIndex;
//                    for (var i = 0; i < $localStorage["todoTasks"].length; i++) {
//                        if (time === $localStorage["todoTasks"][i].time) {
//                            newIndex = i;
//                            console.log(newIndex);
//                            $localStorage["todoTasks"][i].title = editTodo.title;
//                        }
//                    }
//                }
//
//            };
    //refresh the page...

    $scope.refreshMe = function() {
        console.log("refresh");
        $scope.todoList = "";
        $scope.moredata = true;
        skip = 0;
        if ($localStorage["Initializer"]) {
            var checkTodoList = $q.when(ConnectParse.fetchRecord(parseInt($localStorage["Initializer"])), skip);
            checkTodoList.then(
                    function(result) {
                        var mytodolist = [];
                        if (result.length > 0) {
                            if (result.length < 10) {
                                $scope.moredata = false;
                            }
                            for (var i = 0; i < result.length; i++) {
                                var mytodoobj = {};
                                mytodoobj.Unique_Id = result[i].id;
                                mytodoobj.time = result[i].get("time");
                                mytodoobj.alarmTime = result[i].get("alarmTime");
                                mytodoobj.parseStatus = result[i].get("parseStatus");
                                mytodoobj.title = result[i].get("todo_title");
                                mytodoobj.done = result[i].get("done");
                                mytodoobj.position = result[i].get("position");
                                mytodoobj.completed = todostatus[mytodoobj.done];
                                mytodoobj.todoTag = result[i].get("todoTag");
                                mytodolist.push(mytodoobj);
                            }
                            $localStorage['todoTasks'] = mytodolist;
                            $scope.processLocalStorage($localStorage['todoTasks']);
                            skip = skip + 10;
                            $scope.$broadcast('scroll.infiniteScrollComplete');
                            $scope.$broadcast('scroll.resize');
                            $scope.$broadcast('scroll.refreshComplete');
                        }
                        else {
                            $scope.$broadcast('scroll.infiniteScrollComplete');
                            $scope.$broadcast('scroll.refreshComplete');
                            console.log("No records found");
                        }
                    },
                    function(error) {
                        console.log(error);
                        $scope.$broadcast('scroll.infiniteScrollComplete');
                        $scope.$broadcast('scroll.refreshComplete');
                    }
            );
        }
        else if (!$localStorage["Initializer"] && $localStorage['todoTasks']) {
            $scope.$broadcast('scroll.infiniteScrollComplete');
            $scope.$broadcast('scroll.refreshComplete');
            $scope.processLocalStorage($localStorage['todoTasks']);
        }
        if (!$localStorage["loggedUsername"]) {
            //alert("user not loged in anymore!");
            $scope.loggedUser = "";
        }
    };

    //CREATE TODO MODAL FUNCTION
    $ionicModal.fromTemplateUrl('createTodo.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.modal1 = modal;
    });
    $scope.createTodo = function() {
        console.log("create todo");
        //$state.go("app.createTodo");
        $scope.modal1.show();
    };
    $scope.hideCreateModal = function() {
        console.log("Modal Close");
        //$state.go("app.createTodo");
        $scope.mytask.title = "";

        $scope.ChoosenDate = "Date";
        $scope.mytimeTitle = "Time";
        $scope.refreshMe();
        $scope.modal1.hide();
    };

    //EDIT TODO MODAL FUNCTION
    $ionicModal.fromTemplateUrl('editTodo.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.modalEdit = modal;
    });

    $scope.editTodo = function(todoItem) {
        console.log(todoItem);
        $scope.modalEdit.show();
        $scope.editTaskTitle = todoItem.title;
//                $scope.editDate = todoItem.time;
//                $scope.editTime=todoItem.alarmTime;
    };
    
    $scope.closeEditTodo=function(){
        $scope.modalEdit.hide();
        $scope.editTaskTitle=null;
    };
    $ionicModal.fromTemplateUrl('socialShareFB.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.socialShareFb = modal;
    });

    $ionicModal.fromTemplateUrl('socialShareEmail.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.socialShareEmail = modal;
    });

    var sharedTodo;
    $scope.shareTodoOption = function(task) {
        console.log(task);
        sharedTodo = task;
        var actionsheet = $ionicActionSheet.show({
            buttons: [
                {text: '<i class="ion-social-facebook shareIcon shareIconFb"></i><span class="actionButton">Share With Facebook Friends<span>'},
                {text: '<i class="swhats shareIcon shareIconWA" ></i><span class="actionButton">Share With Whatsapp Friends</span>'},
                {text: '<i class="ion-ios-at shareIcon shareIconEmail" ></i><span class="actionButton">Share By Email<span>'}
            ],
            buttonClicked: function(index) {
                if (index == 0) {
                    console.log("index=" + index);
                    console.log("Share Todo Fb");
                    $scope.socialShareFb.show();
                    $scope.fData = $localStorage['fbFrndDetail'];
                    console.log($scope.fData);
                    return true;
                }
                if (index == 1) {
                    console.log("index=" + index);
                    $scope.shareViaWhatsapp(); 
                    return true;
                }
                if (index == 2) {
                    console.log("index=" + index);
                    console.log("Share Todo Email");
                    $scope.socialShareEmail.show();
                    $scope.fetchContact();
                    return true;
                }
            }
        });
        $timeout(function() {
            actionsheet();
        }, 250000);
    };

    $scope.hideFbShareModal = function() {
        $scope.socialShareFb.hide();
    };

    $scope.hideEmailShareModal = function() {
        $scope.socialShareEmail.hide();
    };
    /*----------------------------------------------------------------------------------------------  */
    /*----------------------------------------------------------------------------------------------  */
    /*----------------------------------------------------------------------------------------------  */
    /*---------------------------------ADD-------------------------------------------------------------  */
    /*----------------------------------TODO------------------------------------------------------------  */
    /*-----------------------------------FUNCTION-----------------------------------------------------------  */
    $scope.ChoosenDate = "Date";
    $scope.mytimeTitle = "Time";

    $scope.now = {};
    $scope.now.date;
    $scope.now.time;
    $scope.showReminder = false;
    $scope.userPreTags = [];
    $scope.isshowrem = true;

    $scope.myDates = [
        {title: "Today"},
        {title: "Tomorrow"},
        {title: "Select a date.."}
    ];
    $scope.myTimes = [
        {title: "Morning", dtime: "9:00am"},
        {title: "Afternoon", dtime: "1:00pm"},
        {title: "Evening", dtime: "5:00pm"},
        {title: "Night", dtime: "8:00pm"},
        {title: "Select a time..", dtime: ""}
    ];
    $scope.mytime = $scope.myTimes[0];
    $scope.selectTime = function() {            // select the current time
        console.log("time");
        var timeflag = this.mytimeTitle;
        var date_options = {
            date: new Date(),
            mode: 'time', // or 'date'
            minDate: new Date() - 10000,
            allowOldDates: false,
            allowFutureDates: true,
            doneButtonLabel: 'Change',
            doneButtonColor: '#F2F3F4',
            cancelButtonLabel: 'Default',
            cancelButtonColor: '#000000'
        };
        if (timeflag === "Morning") {
            console.log("morning");
            var d = new Date();
            d.setHours(9);
            d.setMinutes(0);
            $scope.alarmTime = d.getTime();
        }
        if (timeflag === "Afternoon") {
            console.log("aftrenoon");
            var d = new Date();
            d.setHours(13);
            d.setMinutes(0);
            $scope.alarmTime = d.getTime();
        }
        if (timeflag === "Evening") {
            console.log("Evening");
            var d = new Date();
            d.setHours(17);
            d.setMinutes(0);
            $scope.alarmTime = d.getTime();
        }
        if (timeflag === "Night") {
            console.log("night");
            var d = new Date();
            d.setHours(20);
            d.setMinutes(0);
            $scope.alarmTime = d.getTime();
        }
        if (timeflag === "Select a time..") {
            console.log("select time");
            //if its mobile
            if (window.cordova) {
                if (window.cordova) {
                    $cordovaDatePicker.show(date_options).then(function(time) {
                        $scope.alarmTime = time.getTime();
                    });
                }
            }
            //if its desktoop
            else {
                console.log("desktop");
                var d = new Date();
                $scope.alarmTime = d.getTime();
                //$('.clockpicker').clockpicker('show');
            }
        }
    };


    $scope.mydate = $scope.myDates[0];
    $scope.selectDate = function() {            //select the Date..
        var timeflag;
        var date_options = {
            date: new Date(),
            mode: 'date', // or 'time'
            minDate: new Date() - 10000,
            allowOldDates: false,
            allowFutureDates: true,
            doneButtonLabel: 'Change',
            doneButtonColor: '#F2F3F4',
            cancelButtonLabel: 'Default',
            cancelButtonColor: '#000000'
        };
        timeflag = this.mydate.title;
        if (timeflag === "Today") {
            $scope.todoAlarm = new Date().getTime();
        }
        if (timeflag === "Tomorrow") {
            var d = new Date();
            var tday = d.getDate();
            d.setDate(tday + 1);
            $scope.todoAlarm = d.getTime();
        }
        if (timeflag === "Select a date..") {
            console.log("select");
            //if its mobile
            if (window.cordova) {
                $cordovaDatePicker.show(date_options).then(function(date) {
                    $scope.todoAlarm = date.getTime();
                });
            }
            //if its desktoop
            else {
                console.log("desktop");
                $scope.todoAlarm = new Date().getTime();
                //$('.clockpicker').clockpicker('show');
            }
        }
    };

//            if (window.cordova && !$cordovaNetwork.isOnline()) {
//                $state.go('offline');
//            }
    $scope.defaultTime = function() {           // set time to default time...
        console.log("time adjusted");
        var d = new Date();
        var tdate = d.getDate();
        d.setDate(tdate);
        $scope.todoAlarm = d.getTime();
        var t = d.getHours();
        if (t >= 20 && t < 23) {
            d.setDate(tdate + 1);
            $scope.todoAlarm = d.getTime();
            $scope.mydate = $scope.myDates[1];
            $scope.mytime = $scope.myTimes[0];
        }
        else if (t >= 17 && t < 20) {
            $scope.mytime = $scope.myTimes[3];
            d.setHours(20);
            d.setMinutes(0);
            $scope.alarmTime = d.getTime();

        }
        else if (t >= 13 && t < 17) {
            $scope.mytime = $scope.myTimes[2];
            d.setHours(17);
            d.setMinutes(0);
            $scope.alarmTime = d.getTime();

        }
        else if (t >= 9 && t < 13) {
            $scope.mytime = $scope.myTimes[1];
            d.setHours(13);
            d.setMinutes(0);
            $scope.alarmTime = d.getTime();

        }
        else if (t > 0 && t < 9) {
            $scope.mytime = $scope.myTimes[0];
            d.setHours(9);
            d.setMinutes(0);
            $scope.alarmTime = d.getTime();
        }

        // end of curTime

    };
    $scope.$on('$ionicView.enter', function() {     // before enter to ionic view...

        //$scope.todoAlarm = $scope.mydate;
        $scope.defaultTime();
        $scope.curTime = new Date().getTime();

    }); // end of before view change
    var todostatus = {};
    todostatus.true = "item-completed";
    todostatus.false = "item-not-completed";
    // colour picker
    var color = [
        "#FFE6E6",
        "#FFB2B2",
        "#FFFFF0",
        "#F5E0EB",
        "#FAF0F5",
        "#E6FAF0",
        "#CCF5E0",
        "#E6F5FA",
        "#B2E0F0",
        "#FFE0B2",
        "#FFF5E6",
        "#FFF0FF",
        "#FFE6FF",
        "#E0EBEB",
        "#D1E0E0",
    ];
    $scope.addSelf = function(num) {
        var sum = 0;
        while (num > 0)
        {
            sum += num % 10;
            num = Math.floor(num / 10);
        }
        if (sum > 9)
        {
            sum = $scope.addSelf(sum);
        }
        return sum;
    };

    $scope.showrem = function() {
        $scope.isshowrem = false;
    };

    $scope.mytask = {};
    // date picker

    $scope.noReminder = function() {
        $scope.showReminder = false;
        this.showReminder = false;
        $scope.ChoosenDate = "Date";
        $scope.mytimeTitle = "Time";
    };
    $scope.reminderTi$locationme = {};
    var counter = 0;
    $scope.Date = "Select a Date";
    $scope.Time = "Select a Time";
    $scope.options = {
        format: 'yyyy-mm-dd', // ISO formatted date
        onClose: function(e) {
            $state.go("app/createTodo");
        }
    }
    $scope.timeOptions = {
        format: 'HH-i', // ISO formatted date
        onClose: function(e) {
            $state.go("app/createTodo");
        }
    }

    $scope.showDate = function(selection, e) {          // will shoe the Dates to get selected by user..
        if ($scope.reminderTime.isTimeDisplaying === false) {
            $scope.reminderTime.isTimeDisplaying = "true";


        } else {
            $scope.reminderTime.isTimeDisplaying = true;

        }
        //alert("click1");
        e.stopPropagation();
        console.log(selection);
        $scope.ChoosenDate = selection;
        console.log($scope.mydate.title);
        //adjust time
        var d = new Date();
        if (selection === "Today") {
            $scope.todoAlarm = d.getTime();
        }
        if (selection === "Tomorrow") {
            d.setDate(d.getDate() + 1);
            $scope.todoAlarm = d.getTime();
        }

        // $scope.reminderTime.isDateDisplaying = !$scope.reminderTime.isDateDisplaying;
        if (selection === "Select a date..") {
            $scope.pickDate();
        }

    };
    $scope.showTime = function(e, selection) {           // will shoe the Dates to get selected by user..
        //e.preventDefault();
        if ($scope.reminderTime.isTimeDisplaying === "true") {
            $scope.reminderTime.isTimeDisplaying = false;

        } else {
            $scope.reminderTime.isTimeDisplaying = false;

        }

        e.stopPropagation();
        //alert(selection);
        $scope.mytimeTitle = selection;
        var d = new Date();
        if (selection === "Morning") {
            d.setHours(9);
            d.setMinutes(0);
            $scope.alarmTime = d.getTime();

        }
        if (selection === "Afternoon") {
            d.setHours(13);
            d.setMinutes(0);
            d.setDate(d.getDate() + 1);
            $scope.alarmTime = d.getTime();
        }
        if (selection === "Evening") {
            d.setHours(17);
            d.setMinutes(0);
            $scope.alarmTime = d.getTime();
        }
        if (selection === "Night") {
            d.setHours(20);
            d.setMinutes(0);
            d.setDate(d.getDate() + 1);
            $scope.alarmTime = d.getTime();
        }

        if (selection === "Select a time..") {
            $scope.pickTime();
        }
    };
    $scope.pickDate = function() {                              //Date Picker...
        if ($scope.reminderTime.isTimeDisplaying == true) {
            $scope.reminderTime.isTimeDisplaying = false;
        }
        if ($scope.reminderTime.isDateDisplaying == true) {
            $scope.reminderTime.isDateDisplaying = false;
        }
        var date_options = {
            date: new Date(),
            mode: 'date', // or 'time'
            minDate: new Date() - 10000,
            allowOldDates: false,
            allowFutureDates: true,
            doneButtonLabel: 'Change',
            doneButtonColor: '#F2F3F4',
            cancelButtonLabel: 'Default',
            cancelButtonColor: '#000000'
        };
        $cordovaDatePicker.show(date_options).then(function(date) {
            $scope.todoAlarm = date.getTime();
        });
    };
    $scope.reminderTime = {};
    $scope.reminderTime.isTimeDisplaying = false;
    $scope.reminderTime.isDateDisplaying = false;
    $scope.pickTime = function() {                               //time Picker...
        if ($scope.reminderTime.isTimeDisplaying == true) {
            $scope.reminderTime.isTimeDisplaying = false;
        }
        if ($scope.reminderTime.isDateDisplaying == true) {
            $scope.reminderTime.isDateDisplaying = false;
        }
        var time_options = {
            date: new Date(),
            mode: 'time', // or 'date'
            minDate: new Date() - 10000,
            allowOldDates: false,
            allowFutureDates: true,
            doneButtonLabel: 'Change',
            doneButtonColor: '#F2F3F4',
            cancelButtonLabel: 'Default',
            cancelButtonColor: '#000000'
        };
        $cordovaDatePicker.show(time_options).then(function(time) {
            $scope.alarmTime = time.getTime();
        });
    };

    /*--------------------------------SUBMIT--------------------------------------------------------------  */
    /*------------------------------------TODO----------------------------------------------------------  */
    /*--------------------------------------FUNCTION--------------------------------------------------------  */


    $scope.addTodo = function() {                   // Create Todo.....

        if (!$scope.mytask.title || $scope.mytask.title === " " || $scope.mytask.title === "" || $scope.mytask.title.length === 0) {
            if (window.cordova) {
                Alertuser.alert("Please add the task");
                return false;
            }
            else {
                alert("Please add the task");
                return false;
            }
        }
        var val = $scope.mytask.title;
        console.log(new Date($scope.todoAlarm).getHours());

        if ($rootScope.ifdeviceReady && window.cordova) {   //  on Mobile
            console.log("ready");
            //$scope.todoAlarm = date.getTime();
            //console.log($scope.todoAlarm);
            if ($localStorage["Initializer"]) {         // On Mobile When user is Logged In

                var checkTodoList = $q.when(ConnectParse.checkPosition(parseInt($localStorage["Initializer"])));
                checkTodoList.then(
                        function(result) {
                            if (result.length > 0) {
                                newPos = result[0].get("position") + 1;
                            }
                            else {
                                newPos = 0;
                            }

                            var todoObj = {};
                            //poopulate todo item

                            todoObj.title = val;
                            if ($scope.todoAlarm) {
                                todoObj.time = parseInt($scope.todoAlarm);
                                console.log(new Date($scope.todoAlarm).getDate());
                                console.log(new Date(todoObj.time).getDate());
                            }
                            if ($scope.alarmTime) {
                                todoObj.alarmTime = $scope.alarmTime;
                                console.log(new Date($scope.alarmTime).getHours());
                                console.log(todoObj.alarmTime);
                            }
                            todoObj.parseStatus = true;
                            todoObj.done = false;
                            todoObj.userID = parseInt($localStorage["Initializer"]);
                            todoObj.completed = todostatus[todoObj.done];
                            todoObj.position = newPos;

                            console.log(todoObj);
                            //Alertuser.saveAlert("Saved..");
                            var storedTodoTasks = $localStorage['todoTasks'];
                            storedTodoTasks.push(todoObj);
                            $localStorage['todoTasks'] = storedTodoTasks;

                            //$ionicHistory.goBack();
                            $scope.hideCreateModal();
                            Alertuser.saveAlert("Saved..");
                            
                            //$state.go('app.today');
                            //set myToDoObject variable in cloud with values calling parse service
                            var setToDoList = $q.when(ConnectParse.save(todoObj));
                            setToDoList.then(
                                    function(result) {
                                        if (result) {
                                            $scope.mytask.title = "";
                                            console.log(result.title);
                                            //schedule local notification fro saved todo
//                                            window.plugin.notification.local.add({
//                                                id: String(result.id), // A unique id of the notification
//                                                date: new Date($scope.alarmTime), // This expects a date object
//                                                message: result.title, // The message that is displayed
//                                                title: "Reminder", // The title of the message
//                                                autoCancel: true // Setting this flag and the notification is automatically cancelled when the user clicks it
//                                            });
                                                $scope.scheduleSingleNotification = function () {
                                                    $cordovaLocalNotification.schedule({
                                                      id: 1,
                                                      title: 'Title here',
                                                      text: 'Text here',
                                                      data: {
                                                        customProperty: 'custom value'
                                                      }
                                                    }).then(function (result) {
                                                      console.log("notification="+result)
                                                    });
                                                  };
                                                    $rootScope.$on('$cordovaLocalNotification:schedule',
                                                      function (event, notification, state) {
                                                        console.log(notification);
                                                      });
                                        }
                                    },
                                    function(error) {
                                        console.log(error);
                                    }
                            );

                        },
                        function(error) {
                            console.log(error);
                        }
                );
                console.log("Y set" + $scope.todoAlarm);
            } else {                                    // On Mobile When user is Logged Out

                var newPos;
// ravi 
                var result = [];
                //result = $localStorage['todoTasks'];
                if (!$localStorage['todoTasks']) {
                    newPos = 1;
                    console.log("Position :" + newPos);
                }
                else {
                    result = $localStorage['todoTasks'];
                    newPos = result.length + 1;
                    console.log("Position : " + newPos);
                }

                var todoObj = {};
                //poopulate todo item
                todoObj.Unique_Id = "";
                todoObj.title = val;
                if ($scope.todoAlarm) {
                    todoObj.time = parseInt($scope.todoAlarm);
                    console.log(new Date($scope.todoAlarm).getDate());
                    console.log(new Date(todoObj.time).getDate());
                }
                if ($scope.alarmTime) {
                    todoObj.alarmTime = $scope.alarmTime;
                    console.log(new Date($scope.alarmTime).getHours());
                    console.log(todoObj.alarmTime);
                }
                todoObj.parseStatus = true;
                todoObj.done = false;
                // todoObj.userID = parseInt($localStorage["Initializer"]);
                todoObj.completed = todostatus[todoObj.done];
                todoObj.position = newPos;

                console.log(todoObj);

                var storedTodoTasks = [];

                if ($localStorage['todoTasks']) {

                    storedTodoTasks = $localStorage['todoTasks'];
                    storedTodoTasks.push(todoObj);
                    $localStorage['todoTasks'] = storedTodoTasks;
                } else {
                    storedTodoTasks.push(todoObj);
                    $localStorage['todoTasks'] = storedTodoTasks;
                }
                console.log($localStorage['todoTasks']);
                $scope.hideCreateModal();
                Alertuser.saveAlert("Saved..");

                //$ionicHistory.goBack();
                //$state.go('app.today');

                console.log("Y set" + $scope.todoAlarm);
            }
        } else {                                        // On Desktop
            if ($localStorage["Initializer"]) {         // On Desktop when user is Logged in 

                var checkTodoList = $q.when(ConnectParse.checkPosition(parseInt($localStorage["Initializer"])));
                checkTodoList.then(
                        function(result) {
                            if (result.length > 0) {
                                newPos = result[0].get("position") + 1;
                            }
                            else {
                                newPos = 0;
                            }

                            var todoObj = {};
                            //poopulate todo item

                            todoObj.title = val;
                            if ($scope.todoAlarm) {
                                todoObj.time = parseInt($scope.todoAlarm);
                                console.log(new Date($scope.todoAlarm).getDate());
                                console.log(new Date(todoObj.time).getDate());
                            }
                            if ($scope.alarmTime) {
                                todoObj.alarmTime = $scope.alarmTime;
                                console.log(new Date($scope.alarmTime).getHours());
                                console.log(todoObj.alarmTime);
                            }
                            todoObj.parseStatus = true;
                            todoObj.done = false;
                            todoObj.userID = parseInt($localStorage["Initializer"]);
                            todoObj.completed = todostatus[todoObj.done];
                            todoObj.position = newPos;

                            console.log(todoObj);
                            Alertuser.saveAlert("Saved..");
                            var storedTodoTasks = $localStorage['todoTasks'];
                            storedTodoTasks.push(todoObj);
                            $localStorage['todoTasks'] = storedTodoTasks;
                            console.log("here");
                            $scope.hideCreateModal();
                            //$ionicHistory.goBack();
                            //$state.go('app.today');
                            //set myToDoObject variable in cloud with values calling parse service
                            var setToDoList = $q.when(ConnectParse.save(todoObj));
                            setToDoList.then(
                                    function(result) {
                                        if (result) {
                                            // $scope.id=result.Unique_Id;
                                            $scope.mytask.title = "";
                                            console.log(result.title);
                                            //schedule local notification fro saved todo
                                            window.plugin.notification.local.add({
                                                id: String(result.id), // A unique id of the notification
                                                date: new Date($scope.alarmTime), // This expects a date object
                                                message: result.title, // The message that is displayed
                                                title: "Reminder", // The title of the message
                                                autoCancel: true // Setting this flag and the notification is automatically cancelled when the user clicks it
                                            });

                                        }
                                    },
                                    function(error) {
                                        console.log(error);
                                    }
                            );

                        },
                        function(error) {
                            console.log(error);
                        }
                );
                console.log("Y set" + $scope.todoAlarm);
            } else {                                        // On Desktop when user is Logged Out

                var newPos;
// ravi 
                var result = [];
                result = $localStorage['todoTasks'];
                if (!result) {
                    newPos = 1;
                }
                else {
                    newPos = result.length + 1;
                }

                var todoObj = {};
                //poopulate todo item
                todoObj.Unique_Id = "";
                todoObj.title = val;
                if ($scope.todoAlarm) {
                    todoObj.time = parseInt($scope.todoAlarm);
                    console.log(new Date($scope.todoAlarm).getDate());
                    console.log($scope.todoAlarm);
                }
                if ($scope.alarmTime) {
                    todoObj.alarmTime = $scope.alarmTime;
                    console.log(new Date($scope.alarmTime).getHours());
                    console.log(todoObj.alarmTime);
                }
                todoObj.parseStatus = true;
                todoObj.done = false;
                //todoObj.userID = parseInt($localStorage["Initializer"]);
                todoObj.completed = todostatus[todoObj.done];
                todoObj.position = newPos;

                console.log(todoObj);

                var storedTodoTasks = [];

                if ($localStorage['todoTasks']) {
                    storedTodoTasks = $localStorage['todoTasks'];
                    storedTodoTasks.push(todoObj);
                    $localStorage['todoTasks'] = storedTodoTasks;
                } else {
                    storedTodoTasks.push(todoObj);
                    $localStorage['todoTasks'] = storedTodoTasks;
                }

                //Alertuser.saveAlert("desktop--logout--Saved..");
                //$ionicHistory.viewHistory()
                //$ionicHistory.goBack();
                //$state.go('app.today');
                $scope.hideCreateModal();
                console.log("Y set" + $scope.todoAlarm);
            }
        }


    };

    /*----------------------------------------------------------------------------------------------  */
    /*----------------------------------------------------------------------------------------------  */
    /*----------------------------------------------------------------------------------------------  */
    /*---------------------------------TODO-------------------------------------------------------------  */
    /*----------------------------------FACEBOOK SHARE------------------------------------------------------------  */
    /*-----------------------------------FUNCTION-----------------------------------------------------------  */
    $scope.fShareTodo = function(id) {
        alert(id);
        console.log(sharedTodo);
                  var checkUser= $q.when(ConnectParse.fetchFbFrnd(id));
                    checkUser.then(
                        function(result){
                            console.log(result);
                            console.log(result.length);
                            var deviceId=result[0]._serverData.device_id;
                            console.log("received"+JSON.stringify(sharedTodo));
                            var dId={device_id:deviceId,
                            todoTask:sharedTodo};
                            $http.post("http://excellencetechnologies.co.in/pankaj/push.php", dId).success(function(response, status, headers, config) {
                                    if (response.msg === 'Todo Shared Successfully') {
                                        alert("success");
                                    } else {
                                        alert(response.error);
                                    }
                                }).error(function(data, status) { // called asynchronously if an error occurs
                                    // alert("fail") ;                         
                                    alert(status);
                                });
                        },
                        function(error){
                            console.log(error);
                        }
                    );
    };
    
    /*----------------------------------------------------------------------------------------------  */
    /*----------------------------------------------------------------------------------------------  */
    /*----------------------------------------------------------------------------------------------  */
    /*---------------------------------TODO-------------------------------------------------------------  */
    /*----------------------------------FETCH CONTACT------------------------------------------------------------  */
    /*-----------------------------------FUNCTION-----------------------------------------------------------  */
$scope.fetchContact=function(){
  document.addEventListener("deviceready", onDeviceReady, false);      // device APIs are available

    function onDeviceReady() {
        // specify contact search criteria
        var options = new ContactFindOptions();
        options.filter = "";                        // empty search string returns all contacts
        options.multiple = true;                        // return multiple results
        var filter = ['displayName', 'phoneNumbers', 'photos', 'emails']; // return contact.displayName, phoneNumbers ..... email field

        // find contacts
        navigator.contacts.find(filter, onSuccess, onError, options);
    }

    var ContactDetails = [];
    var newContact = [];

    // onSuccess: Get the current contacts having Email

    function onSuccess(contacts) {

        for (var i = 0; i < contacts.length; i++) {
            if (contacts[i].emails) {
                var diary = {};

                for (var j = 0; j < contacts[i].emails.length; j++) {       // get the Email id
                    diary.email = contacts[i].emails[j].value;
                }
                if (!contacts[i].displayName) {                  // many contacts don't have displayName
                    diary.name = "No name";
                    //console.log("un " + i + diary.name);
                }
                else {
                    diary.name = contacts[i].displayName;
                    // console.log(diary.name);
                    // console.log(i + contacts[i].displayName);
                }

                var randColor = ConnectParse.getRandomColor();      // generate the random color.
                diary.color = randColor;

                //diary.email = contacts[i].emails;
                // console.log(diary);
                ContactDetails.push(diary);

            } else {
                // alert("no email..." + contacts[i].displayName);
            }
        }

        // console.log(ContactDetails);
        // console.log(ContactDetails.length);

        var index = 0;
        for (var j = 0; j < ContactDetails.length; j++) {
            //alert("done");
            var newDiary = {};
            if (ContactDetails[j].name === "No name") {
                //alert(ContactDetails[j].name);
            } else {
                // alert(ContactDetails[j]);
                //console.log(index + " Index " + ContactDetails[j].name + j);
                newDiary.name = ContactDetails[j]['name'];
                newDiary.color = ContactDetails[j]['color'];
                newDiary.email = ContactDetails[j]['email'];
                // console.log(newDiary);
                newContact.push(newDiary);
                index++;
            }

            // console.log(newContact);
        }

        $scope.myContact = newContact;                       // pushing the email contact to $scope variable.
        //console.log(newContact);
        console.log("myContact : " + JSON.stringify($scope.myContact));
    }

    function onError(contactError) {
        alert('onError!');
    }

};
/*----------------------------------------------------------------------------------------------  */
    /*----------------------------------------------------------------------------------------------  */
    /*----------------------------------------------------------------------------------------------  */
    /*---------------------------------SEND TODO-------------------------------------------------------------  */
    /*-------------------------------------THROUGH EMAIL---------------------------------------------------------  */
    /*-----------------------------------FUNCTION-----------------------------------------------------------  */
$scope.eShareTodo=function(emailID){
    console.log(emailID);
    console.log(sharedTodo);
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        
        if (reg.test(emailID) === true)
        {
            var confirm = $ionicPopup.confirm({
                title: 'Share',
                template: 'Sahre with ' + emailID,
                okText: "Yes!",
                cancelText: "Cancel"
            });
            confirm.then(function(res) {
                if (res) {
                    init();//INITIATE SHORT URL API
                     var todoString = JSON.stringify(sharedTodo);
                     var encodeTodo = encodeURIComponent(todoString);

                     var todoUrl = 'http://excellencetechnologies.co.in/ravikant/todoApp/App.php?task=' + encodeTodo + '';
                     console.log(todoUrl);
                     
                     //FUNCTION TO MAKE URL SHORT
                     function makeRequest() {
                        var request = gapi.client.urlshortener.url.insert({
                          'resource': {
                                'longUrl': todoUrl
                              }
                        });
                        request.then(function(response) {
                          console.log(response.result.id);
                           $localStorage['eShortUrl']=" ";
                           $localStorage['eShortUrl']=response.result.id;
                           console.log($localStorage['eShortUrl']);
                        }, function(reason) {
                          console.log('Error: ' + reason.result.error.message);
                        });
                      }
                     function init() {
                        gapi.client.setApiKey('AIzaSyBP0C4zOlMFFEOWZnK9oGIkUITACR0vNXg');
                        gapi.client.load('urlshortener', 'v1').then(makeRequest);
                      }
                      
                      
                    var emailData = {
                        subject: 'TodoApp',
                        to: emailID,
                        message:$localStorage['eShortUrl']
                    };
                    console.log(emailData);
                    var checkUser = $q.when(ConnectParse.checkIfUserExist(emailID));
                    checkUser.then(
                            function(result) {
                                console.log(result);
                                console.log(result.length);
                                if (result.length === 0) {
                                    $http.post("http://excellencetechnologies.co.in/ravikant/send_email.php", emailData).success(function(response, status, headers, config) {
                                        if (response.msg === 'Email send Successfully!') {
                                            Alertuser.saveAlert(response.msg);
                                            console.log(response);    
                                        } else {
                                            Alertuser.saveAlert(response.error);
                                        }
                                    }).error(function(data, status) { // called asynchronously if an error occurs
                                        // alert("fail") ;                         
                                        Alertuser.saveAlert(status);
                                    });
                                } else {
                                    console.log("user found");
                                    console.log(JSON.stringify(result));
                                    
                                        var deviceId=result[0]._serverData.device_id;
                                        console.log("received"+JSON.stringify(sharedTodo));
                                        var dId={device_id:deviceId,
                                                    todoTask:sharedTodo};
                                        $http.post("http://excellencetechnologies.co.in/pankaj/push.php", dId).success(function(response, status, headers, config) {
                                                if (response.msg === 'Todo Shared Successfully') {
                                                    alert("success");
                                                } else {
                                                    alert(response.error);
                                                }
                                            }).error(function(data, status) { // called asynchronously if an error occurs
                                                // alert("fail") ;                         
                                                console.log(status);
                                            });
                                    
                                    };
                                }
                            );


                } else {
                    // Don't close  
                }
                
            });
        }
    
};
/*----------------------------------------------------------------------------------------------  */
    /*----------------------------------------------------------------------------------------------  */
    /*----------------------------------------------------------------------------------------------  */
    /*---------------------------------DELETE------------------------------------------------------------  */
    /*-------------------------------------TODO---------------------------------------------------------  */
    /*-----------------------------------FUNCTION-----------------------------------------------------------  */
    
    $scope.deleteTodo=function(todo){
        console.log(todo);
        if($localStorage['Initializer']){
            console.log(todo);
            var checkTodoList = $q.when(ConnectParse.checkIfRecordExist(parseInt($localStorage["Initializer"])));
                checkTodoList.then(
                        function(result) {
                            console.log(result);
                            var newIndex;
                            if (result.length > 0) {
                                console.log(result.length);
                                for (var i = 0; i < result.length; i++) {
                                    var objid = result[i].id;
                                    console.log(objid);
                                    if (todo.taskId === objid) {
                                        newIndex = i;
                                        console.log(newIndex);
                                        var deletedData={
                                            'userID':$localStorage['Initializer'],
                                            'time':todo.time,
                                            'title':todo.title,
                                            'alarmTime':todo.alarmTime
                                        };
                                        
                                        var setToDoList = $q.when(ConnectParse.saveDeletedTask(deletedData));
                                            setToDoList.then(
                                                    function(result) {
                                                        console.log(result);
                                                    });
                                        var deleteRecord = $q.when(ConnectParse.deleteRecord(result[i]));
                                        deleteRecord.then(
                                                function(result) {
                                                    console.log("deleted");
                                                    $scope.refreshMe();
                                                }, function(error) {
                                            console.log("Not deleted");
                                        });
                                    }
                                }
                            }
                            else {
                                console.log("No records found");
                            }
                        },
                        function(error) {
                            console.log(error);
                        }
                );
        }
        else
        {
            if($localStorage['deleteTodo']){
                var todoArray=[];
                todoArray.push(todo);
                $localStorage['deleteTodo']=todoArray;
                console.log($localStorage['deleteTodo']);
            }
            else{
                $localStorage['deleteTodo']=todo;
                 console.log($localStorage['deleteTodo']);
            }
        }
    };
});


app.directive('focusMe', function($timeout, $parse) {
    return {
        link: function(scope, element, attrs) {
            var model = $parse(attrs.focusMe);
            scope.$watch(model, function(value) {
                console.log("search Focused");
                if (value === true) {
                    $timeout(function() {
                        element[0].focus();
                    });
                }
            });

        }
    };
});