angular.module('app.controller', [])

        .controller('AppCtrl', function($scope, $http, $ionicHistory, Alertuser, $timeout, $cordovaNetwork, ConnectParse, $q, $state, $rootScope, $localStorage, $ionicLoading) {
            $scope.loggedUser = $localStorage["loggedUsername"];
            $scope.loggedName=$localStorage["loggedName"];
            $scope.loggedUserPic=$localStorage["loggedUserPic"];
            $scope.$on('$ionicView.enter', function() {         // when Enter to the Ionic View

                if ($localStorage["Initializer"]) {
                    $rootScope.ifLoogedIn = true;
                    
                }
//              
                $scope.login = function() {     //Login Function
                    $state.go("login");
                };
//      
                $scope.logout = function() {                // LogOut Function
                    console.log("logging out");
                    $ionicLoading.show({
                        content: '<i class="icon ion-loading-c">',
                        animation: 'fade-in',
                        showBackdrop: true,
                        maxWidth: 100,
                        showDelay: 0
                    });

                    delete $localStorage["Initializer"];
                    delete $localStorage["loggedUsername"];
                    console.log("a");
                    //delete $localStorage["todoTasks"];
                    //delete $localStorage["userTags"];
                    //delete $localStorage["loggedUsername"];
                    $timeout(function() {
                        $ionicLoading.hide();
                        //$location.path("/app/add_todo");

                    }, 2000);
                    Alertuser.alert("Logging out..");
                    $rootScope.ifLoogedIn = false;
//                console.log("b"+$scope.ifLoogedIn);
                    $ionicHistory.goBack();
                    // $state.go('app.addTodo');
                    //$state.go('app.today');
                    console.log("c");
                }; // end of logout function
            });
        });



