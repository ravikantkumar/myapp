angular.module('starter.services', [])
        .service("Data", function() {
            var tasks = [];
            var taskObj = {};
            taskObj.title = "";
        })
        .service("Alertuser", function() {
            this.alert = function(msg) {
                console.log(msg);
                if (window.plugins) {
                    window.plugins.toast.showLongBottom(msg, function(a) {
                        console.log("msg");
                    }, function(b) {
                    });
                }

            };
            this.saveAlert = function(msg) {
                //alert(msg);
                if (window.plugins) {
                    window.plugins.toast.showShortCenter(msg, function(a) {
                        console.log(msg);
                    }, function(b) {
                    });
                }

            };
        })
//        .service("fbFrndPrfPic",function(){
//             fbFrndId=function(id){
//                 alert(id);
//               console.log(id);
//             };
//        })
        .service("ConnectParse", function($q,$localStorage) {
            var service = {};
            var User = Parse.Object.extend("AppUsers");
            var ToDoObject = Parse.Object.extend("ToDoObject");
            var FBAppUser = Parse.Object.extend("FBAppUser");
            var deletedObj=Parse.Object.extend("deletedTask");
            
            service.UserSave = function(userObj) {
               // console.log(todoObj);
                //var ToDoObject1 = Parse.Object.extend("ToDoObject1");
                var parseObj = new User();
                parseObj.set("user_id", parseInt(userObj.id));
                parseObj.set("f_name", String(userObj.fname));
                parseObj.set("l_name", String(userObj.lname));
                parseObj.set("gender", String(userObj.gender));
                parseObj.set("profile_link", String(userObj.profile));
                parseObj.set("email", String(userObj.email));
                parseObj.set("channel", Array("ch_"+userObj.id));
                var promiseObject = $q.defer();
                parseObj.save(null, {
                    success: function(data) {
                        console.log("succ");
                        // Execute any logic that should take place after the object is saved.
                        promiseObject.resolve(data);
                    },
                    error: function(data, error) {
                        console.log("err");
                        // Execute any logic that should take place if the save fails.
                        // error is a Parse.Error with an error code and message.
                        promiseObject.reject(error.message);
                    }
                });
                return promiseObject.promise;
            };
            
//            service.fbSave = function(todoObj) {
//                console.log(todoObj);
//                //var ToDoObject1 = Parse.Object.extend("ToDoObject1");
//                var parseObj = new FBAppUser();
//                parseObj.set("fbid", parseInt(todoObj.id));
//                parseObj.set("fname", String(todoObj.fname));
//                parseObj.set("lname", String(todoObj.lname));
//                parseObj.set("gender", String(todoObj.gender));
//                parseObj.set("profile", String(todoObj.profile));
//                var promiseObject = $q.defer();
//                parseObj.save(null, {
//                    success: function(data) {
//                        console.log("succ");
//                        // Execute any logic that should take place after the object is saved.
//                        promiseObject.resolve(data);
//                    },
//                    error: function(data, error) {
//                        console.log("err");
//                        // Execute any logic that should take place if the save fails.
//                        // error is a Parse.Error with an error code and message.
//                        promiseObject.reject(error.message);
//                    }
//                });
//                return promiseObject.promise;
//            };

            service.save = function(todoObj) {                      // save the data...
                //var ToDoObject1 = Parse.Object.extend("ToDoObject1");
                var parseObj = new ToDoObject();
                if(todoObj.userID){
                   parseObj.set("userID", parseInt(todoObj.userID)); 
                }else{
                    var userid=$localStorage["Initializer"];
                    parseObj.set("userID", parseInt(userid)); 
                }
                //parseObj.set("userID", parseInt(todoObj.userID));
                parseObj.set("parseStatus", false);
                parseObj.set("done", todoObj.done);
                if (todoObj.time) {
                    parseObj.set("time", todoObj.time);
                }
                if (todoObj.alarmTime) {
                    parseObj.set("alarmTime", todoObj.alarmTime);
                }
                parseObj.set("todo_title", todoObj.title);
                parseObj.set("position", todoObj.position);
                parseObj.set("todoTag", todoObj.todoTag);
                var promiseObject = $q.defer();
                parseObj.save(null, {
                    success: function(data) {
                        // Execute any logic that should take place after the object is saved.
                        var ifRetrieved = service.retrieve(data.id);
                        ifRetrieved.then(
                                function(result) {
                                    console.log(result);
                                    promiseObject.resolve(result);
                                },
                                function(error) {
                                    promiseObject.reject(error);
                                }
                        );

                    },
                    error: function(data, error) {
                        // Execute any logic that should take place if the save fails.
                        // error is a Parse.Error with an error code and message.
                        promiseObject.reject(error.message);
                    }
                });
                return promiseObject.promise;
            };
            
            service.saveDeletedTask=function(deletedTask){
                var parseObj = new deletedObj();
                 parseObj.set("userId", parseInt(deletedTask.userID));
                 parseObj.set("parseStatus", false);
                 parseObj.set("time", deletedTask.time);
                 parseObj.set("todoTitle", deletedTask.title);
                 parseObj.set("alarmTime", deletedTask.alarmTime);
                 var promiseObject = $q.defer();
                parseObj.save(null, {
                    success: function(data) {
                        // Execute any logic that should take place after the object is saved.
                        console.log(data);
                        var ifRetrieved = service.retrieveDeletedTask(data.id);
                        ifRetrieved.then(
                                function(result) {
                                    console.log(result);
                                    promiseObject.resolve(result);
                                },
                                function(error) {
                                    promiseObject.reject(error);
                                }
                        );

                    },
                    error: function(data, error) {
                        // Execute any logic that should take place if the save fails.
                        // error is a Parse.Error with an error code and message.
                        promiseObject.reject(error.message);
                    }
                });
                return promiseObject.promise;
                 
            };
            service.MultipleSave = function(localData, user_id) {       // save the $localStorage["todoTasks"] when User logged in
                //alert("multiple Save");
                console.log(localData);
                console.log(localData.length);

                var a = 0;
                //var ToDoObject1 = Parse.Object.extend("ToDoObject1");
                for (var i = 0; i < localData.length; i++) {
                    if (localData[i].Unique_Id === "") {
                        // alert(localData[i].title);
                        a = 1;
                        var parseObj = new ToDoObject();
                        parseObj.set("userID", parseInt(user_id));
                        parseObj.set("parseStatus", false);
                        parseObj.set("done", localData[i].done);
                        if (localData[i].time) {
                            parseObj.set("time", localData[i].time);
                        }
                        if (localData[i].alarmTime) {
                            parseObj.set("alarmTime", localData[i].alarmTime);
                        }
                        parseObj.set("todo_title", localData[i].title);
                        parseObj.set("position", localData[i].position);


                        var promiseObject = $q.defer();
                        parseObj.save(null, {
                            success: function(data) {
                                // Execute any logic that should take place after the object is saved.
                                promiseObject.resolve(data);

                            },
                            error: function(data, error) {
                                // alert("plz check your internet Connection...");
                                // Execute any logic that should take place if the save fails.
                                // error is a Parse.Error with an error code and message.
                                promiseObject.reject(error.message);
                            }
                        });
                    }

                }
                if (a === 0) {
                    //alert("No Data to Save...");
                }else{
                    return promiseObject.promise;
                }
                
            };
            service.retrieve = function(parseObjID) {
                var myToDoQuery = new Parse.Query(ToDoObject);
                var myResponse = $q.defer();
                var myDataObj = {};
                myToDoQuery.get(parseObjID, {
                    success: function(responseObj)
                    {
                        // The object was retrieved successfully.
                        myDataObj.Unique_Id = responseObj.id;
                        myDataObj.time = responseObj.get("time");
                        myDataObj.parseStatus = responseObj.get("parseStatus");
                        myDataObj.title = responseObj.get("todo_title");
                        myDataObj.done = responseObj.get("done");
                        myResponse.resolve(myDataObj);
                    },
                    error: function(error) {
                        // The object was not retrieved successfully.
                        // error is a Parse.Error with an error code and message.
                        myResponse.reject(error.message);

                    }
                });
                return myResponse.promise;
            };
            
            service.retrieveDeletedTask = function(parseObjID) {
                var myToDoQuery = new Parse.Query(deletedObj);
                var myResponse = $q.defer();
                var myDataObj = {};
                myToDoQuery.get(parseObjID, {
                    success: function(responseObj)
                    {
                        // The object was retrieved successfully.
                        myDataObj.Unique_Id = responseObj.id;
                        myDataObj.time = responseObj.get("time");
                        myDataObj.parseStatus = responseObj.get("parseStatus");
                        myDataObj.title = responseObj.get("todo_title");
                        myDataObj.done = responseObj.get("done");
                        myResponse.resolve(myDataObj);
                    },
                    error: function(error) {
                        // The object was not retrieved successfully.
                        // error is a Parse.Error with an error code and message.
                        myResponse.reject(error.message);

                    }
                });
                return myResponse.promise;
            };
            
            
            service.update = function(parseObj, toUpdateField, toUpdateobj) {
                var toUpdateValue = toUpdateobj.title;
                var promiseObject = $q.defer();
                parseObj.save(null, {
                    success: function(data) {

                        // Execute any logic that should take place after the object is saved.
                        parseObj.set(toUpdateField, toUpdateValue);
                        parseObj.save();
                        promiseObject.resolve(data);
                    },
                    error: function(data, error) {
                        promiseObject.reject(error.message);
                    }
                });
                return promiseObject.promise;
            };
            service.complete = function(parseObj, toUpdateField, doneStatus) {
                var toUpdateValue = doneStatus;
                var promiseObject = $q.defer();
                parseObj.save(null, {
                    success: function(data) {

                        // Execute any logic that should take place after the object is saved.
                        parseObj.set(toUpdateField, toUpdateValue);
                        parseObj.save();
                        promiseObject.resolve(data);
                    },
                    error: function(data, error) {
                        promiseObject.reject(error.message);
                    }
                });
                return promiseObject.promise;
            };
            service.checkQuery = function(parseDatatype, fieldName, fieldValue) {
                var promiseObject = $q.defer();
                var query = new Parse.Query(parseDatatype);
                query.equalTo(fieldName, fieldValue);
                query.find({
                    success: function(results) {
                        promiseObject.resolve(results);
                    },
                    error: function(error) {
                        promiseObject.reject(error.message);
                    }
                });
                return promiseObject.promise;

            };
            service.checkIfUserExist = function(useremail) {
                var promiseObject = $q.defer();
                var query = new Parse.Query(User);
                query.equalTo("email", String(useremail));
                // query.descending("position");
                query.find({
                    success: function(results) {
                        promiseObject.resolve(results);
                    },
                    error: function(error) {
                        promiseObject.reject(error.message);
                    }
                });
                return promiseObject.promise;

            };
            
            
            service.checkIfRecordExist = function(userid) {
                var promiseObject = $q.defer();
                var query = new Parse.Query(ToDoObject);
                query.equalTo("userID", parseInt(userid));
                query.descending("position");
                query.find({
                    success: function(results) {
                        promiseObject.resolve(results);
                    },
                    error: function(error) {
                        promiseObject.reject(error.message);
                    }
                });
                return promiseObject.promise;

            };
            service.checkPosition = function(userid) {
                console.log(userid);
                var promiseObject = $q.defer();
                var query = new Parse.Query(ToDoObject);
                query.equalTo("userID", parseInt(userid));
                query.descending("position");
                query.find({
                    success: function(results) {
                        console.log(results);
                        promiseObject.resolve(results);
                    },
                    error: function(error) {
                        promiseObject.reject(error.message);
                    }
                });
                return promiseObject.promise;

            };
            service.fetchRecord = function(userid, userskip) {
                var promiseObject = $q.defer();
                var query = new Parse.Query(ToDoObject);
                query.equalTo("userID", parseInt(userid));
//                query.limit(10);
                query.skip(userskip);
                query.descending("position");
                query.find({
                    success: function(results) {
                        promiseObject.resolve(results);
                    },
                    error: function(error) {
                        promiseObject.reject(error.message);
                    }
                });
                return promiseObject.promise;

            };
            service.fetchDeletedRecord=function(userid,userskip){
                var promiseObject = $q.defer();
                var query = new Parse.Query(deletedObj);
                query.equalTo("userId", parseInt(userid));
//                query.limit(10);
                query.skip(userskip);
                query.descending("position");
                query.find({
                    success: function(results) {
                        promiseObject.resolve(results);
                    },
                    error: function(error) {
                        promiseObject.reject(error.message);
                    }
                });
                return promiseObject.promise;
            };
            
            service.deleteRecord = function(parseObj) {
                var promiseObject = $q.defer();
                parseObj.destroy({
                    success: function(data) {
                        // Execute any logic that should take place after the object it is deleted.
                        promiseObject.resolve(data);
                    },
                    error: function(data, error) {
                        // Execute any logic that should take place if the save fails.
                        // error is a Parse.Error with an error code and message.
                        promiseObject.reject(error.message);
                    }
                });
                return promiseObject.promise;
            };

            service.getRandomColor = function() {
                var colors = ['#f44336', '#e91e63', '#9c27b0', '#673ab7', '#3f51b5', '#2196f3', '#03a9f4', '#00bcd4', '#009688', '#4caf50', '#8bc34a', '#cddc39', '#ffeb3b', '#ffc107', '#ff9800', '#ff5722', '#795548', '#9e9e9e', '#607d8b'];
                var index = Math.floor(Math.random() * (colors.length + 1));
                var color = colors[index];
                if (!color) {
                    color = colors[0];
                }
                return color;
            };

            service.sendNotification = function(channel) {
                var data = {};
                data.title = "TodoApp";
                data.msg = "Shared a todo";
                var channel = channel;
                var expiry = new Date().getTime() + expiry * 1000;
                var defer = $q.defer();
                Parse.Push.send({
                    channels: [channel],
                    data: data,
                    expiration_time: new Date(expiry)
                }, {
                    success: function(result) {
                        defer.resolve(result);
                    },
                    error: function(error) {
                        console.error('send alert error ');
                        console.error(error);
                        defer.reject(error);
                    }
                });
                return defer.promise;
            };
            
//            service.fetchFbFrnd=function(frndId){
//                alert("Friend Id="+frndId);
//                var pobject=$q.defer();
//                var query=new Parse.Query(Parse.Object.extend("AppUsers"));
//                query.equalTo("user_id",frndId);
//                query.first({
//                    success: function(fResult){
//                        alert("length="+fResult);
//                        console.log(fResult);
//                        pobject.resolve(fResult);
//                    },
//                    error: function(error) {
//                        alert("Error: " + error.code + " " + error.message);
//                      }
//                });
//                return pobject.promise;
//            };
            
            service.fetchFbFrnd = function(userid) {
                var promiseObject = $q.defer();
                var query = new Parse.Query(User);
                query.equalTo("user_id", parseInt(userid));
                // query.descending("position");
                query.find({
                    success: function(results) {
                        promiseObject.resolve(results);
                    },
                    error: function(error) {
                        promiseObject.reject(error.message);
                    }
                });
                return promiseObject.promise;

            };
            
            service.saveDevice = function(dId,userid) {
                var promiseObject = $q.defer();
                var query = new Parse.Query(User);
                query.equalTo("user_id", parseInt(userid));
                // query.descending("position");
                query.first({
                    success: function(results) {
                        console.log(results);
                        results.set("device_id",dId);
                        results.save();
                    },
                    error: function(error) {
                        promiseObject.reject(error.message);
                    }
                });
                return promiseObject.promise;

            };
            
            

            return service;

        });