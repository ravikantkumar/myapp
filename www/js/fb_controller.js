angular.module('fb.controller', [])
        .controller('FbCtrl', function($ionicPlatform, $ionicHistory, $ionicLoading, $ionicPopup, $cordovaNetwork, $scope, $state, $rootScope, $q, ConnectParse, Alertuser, $localStorage) {
            var self = this;
            /*if (!$rootScope.ifdeviceReady) {
             alert("Please re launch the application");
             }*/

            var backButton = $ionicPlatform.registerBackButtonAction(function(e) {

                e.preventDefault();
                previousView();
                // var a=1;
                function previousView() {                       // Back Button Functionality
                    var confirmPopup = $ionicPopup.confirm({
                        title: 'Confirm',
                        template: 'This will redirect u to home page! Are you sure you wanna back to home?',
                        okText: "Yes!",
                        cancelText: "Cancel"
                    });
                    confirmPopup.then(function(res) {
                        if (res) {
                            $state.go("app.addTodo");

                        } else {
                            // Don't close  
                        }
                    });
                }

                return false;
            }, 1000);

            $scope.$on('$destroy', backButton);         // destroy  Back Button Functionality


            $scope.$on('$ionicView.enter', function() {
                if ($localStorage["Initializer"]) {
                    $state.go("app.addTodo");
                }
            });

            $scope.gLogin = function() {
                //alert("not implemented yet");
            };
            var userDetails=[];
            var userObj={};
            $scope.fbLogin = function() {               //Facebook Login...and Save the $localStorage["todoTasks"]
                var LocalStorageTags = [];
                if (window.cordova) {
                    alert("face");
                    
                    facebookConnectPlugin.login(["public_profile", "email"], function(response) {
                        alert("facebook");
                        if (response.authResponse) {
                            
                            console.log(JSON.stringify(response));
                            self.processFacebook();
//                             fbFrndfetch();
                            console.log(response.authResponse.userID);
                            var UserId = response.authResponse.userID;
                            //alert(UserId);
                            userObj.userId=UserId;
                            LocalStorageTags = $localStorage["todoTasks"];
                            console.log(LocalStorageTags);
                            var setToDoList = $q.when(ConnectParse.MultipleSave(LocalStorageTags, UserId));
                            setToDoList.then(
                                    function(result) {
                                        if (result) {
                                            Alertuser.saveAlert("Tasks saved to Server...");
                                            // $ionicHistory.goBack();
                                            $state.go("app.addTodo");
                                        }else{
                                            Alertuser.saveAlert("No New Tasks...");
                                            // $ionicHistory.goBack();
                                            $state.go("app.addTodo");
                                        }
                                    },
                                    function(error) {
                                        console.log(error);
                                        Alertuser.saveAlert(error);
                                    });
                        } else {
                            // user is not logged in

                            console.log("failed");
                        }
                    });
                }
                //for desktop
                else {
                    $rootScope.ifdeviceReady = true;
                    $localStorage["loggedUsername"] = "meanishasingh@yahoo.com";
                    $localStorage["Initializer"] = 348139555381867;
                    var UserId = $localStorage["Initializer"];
                    // delete $localStorage["todoTasks"];
                    LocalStorageTags = $localStorage["todoTasks"];
                    var setToDoList = $q.when(ConnectParse.MultipleSave(LocalStorageTags, UserId));
                    setToDoList.then(
                            function(result) {
                                if (result) {
                                    alert("Data saved....");
                                    //$state.go("app.addodo");
                                }
                            },
                            function(error) {
                                console.log(error);
                                alert("error");
                            }
                    );
                    $state.go("app.addTodo");
                }
            };
            self.processFacebook = function() {                             // get Userinfo from facebook.
                facebookConnectPlugin.api('/me?fields=id,name,first_name,last_name,gender,link,email,picture', null, function(response) {
                    if (response.error) {
                        console.log('Unexpected Facebook Login Error: ' + JSON.stringify(response.error));
                        Alertuser.alert('Error! Try Again..');
                    } else {
                        if (response) {
                            //show msg user logging in bottom
                            Alertuser.alert("Logging in..");
                            fbFrndfetch(); //FETCH FACEBOOK FRIEND LIST FUNCTION 
                            var facebookObj = {};
                            facebookObj.id = response.id;
                            facebookObj.fname = response.first_name;
                            facebookObj.lname = response.last_name;
                            facebookObj.gender = response.gender;
                            facebookObj.profile = response.link;
                            facebookObj.email = response.email;
                            $localStorage["loggedUsername"] = facebookObj.email;
                            $localStorage["loggedName"]=response.name;
                            $localStorage["loggedUserPic"]=response.picture.data.url;
                            // save to your db or apply ur custom logic
                            console.log(facebookObj.email);
                            $localStorage["Initializer"] = facebookObj.id;
                            var checkUser= $q.when(ConnectParse.checkIfUserExist(facebookObj.email));
                            checkUser.then(
                                    function(result){
                                        console.log(result);
                                        console.log(result.length);
                                        if(result.length===0){
                                            var userSave = $q.when(ConnectParse.UserSave(facebookObj));
                                            userSave.then(
                                                    function(result){
                                                         console.log(result);
                                                    },function(error){
                                                       console.log(error); 
                                                    });
                                        }
                                    },
                                    function(error){
                                        console.log(error);
                                    });
                        }//end of if
                    }//end of else
                });
                //return  $localStorage["Initializer"];
            };//end of processfacebook()
            fbFrndfetch=function(){
                var frndData=[];
                facebookConnectPlugin.api('/me/friends?fields=name,picture', null, function(f_response) {
                    var tData=f_response.data.length;
                     console.log(JSON.stringify(f_response));
                     console.log(tData);
                     console.log(f_response.data[0].name + f_response.data[0].id);
                     for(var i=0;i<tData;i++){
                         var obj={};
                         var fName=f_response.data[i].name;
                         var fId=f_response.data[i].id;
                         var prfPicUrl=f_response.data[i].picture.data.url;

                         obj["name"]=fName;
                         obj["id"]=fId;
                         obj["pic"]=prfPicUrl;
                         console.log(obj)
                         frndData.push(obj);
                         //console.log(frndData)
                     };
                     console.log(frndData);
//                                $scope.fData=frndData;
//                                console.log($scope.fData);
                     $localStorage['fbFrndDetail']=frndData;
                     console.log($localStorage['fbFrndDetail']);
                 });
             };//end of fbFrnd
        }); // end of todo controller  

