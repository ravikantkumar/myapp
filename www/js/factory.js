var app=angular.module('starter.factory', []);
       app.factory('PushProcessingService',function(ConnectParse,$q,$state,$ionicPopup,$localStorage){
            function onDeviceReady(){
                console.log('NOTIFY  Device is ready.  Registering with GCM server');
                //alert('NOTIFY  Device is ready.  Registering with GCM server');
                var pushNotification = window.plugins.pushNotification;
                pushNotification.register(successHandler, errorHandler, {
                    "senderID": "329544721548",
                    "ecb": "onNotification"
                }); // required!
            }
            function successHandler(result) {
                console.log('NOTIFY  pushNotification.register succeeded.  Result = ' + result);
                //alert('NOTIFY  pushNotification.register succeeded.  Result = ' + result)
            }
            function errorHandler(error) {
                console.error('NOTIFY  ' + error);
            }
            return {
                initialize: function() {
                    console.log('NOTIFY  initializing');
                    document.addEventListener('deviceready', onDeviceReady, false);
                },
                registerID: function(regid) {
                    //Insert code here to store the user's ID on your notification server. 
                    //You'll probably have a web service (wrapped in an Angular service of course) set up for this.  
                    facebookConnectPlugin.getLoginStatus(function(response){
                        if(response.status === 'connected'){
                            var uid = response.authResponse.userID;
                            alert("facebook UID="+uid);
                            var savedevice = $q.when(ConnectParse.saveDevice(regid,uid));
                                savedevice.then(
                                    function(result) {
                                        if (result) {
                                            console.log(result);
                                            
                                        }
                                    },
                                    function(error) {
                                        console.log(error);
                                    }
                            );
                        }
                    });

                },
                receivedToDo: function(todo){
                    //alert(todo);
                    if(todo.length!=0){
                        var confirm = $ionicPopup.confirm({
                            title: 'Received Task',
                            template: 'You have received a task '+todo.title+'.Do you want to add it ' ,
                            okText: "Yes",
                            cancelText: "No"
                        });
                        confirm.then(function(res) {
                            if(res){
                                if ($localStorage["Initializer"]){
                                    var savetodo = $q.when(ConnectParse.save(todo));
                                        savetodo.then(
                                            function(result) {
                                                if (result) {
                                                    console.log(result);
                                                    $state.go("app.addTodo");
                                                }
                                            },
                                            function(error) {
                                                console.log(error);
                                            }
                                    );
                                }
                                else{
                                    var storedTodoTasks = [];
                                    if ($localStorage['todoTasks']) {

                                        storedTodoTasks = $localStorage['todoTasks'];
                                        storedTodoTasks.push(todo);
                                        $localStorage['todoTasks'] = storedTodoTasks;
                                    } else {
                                        storedTodoTasks.push(todo);
                                        $localStorage['todoTasks'] = storedTodoTasks;
                                    }
                                    console.log($localStorage['todoTasks']);
                                }
                            }
                        })
                    }
                    
                },
                //unregister can be called from a settings area.
                unregister: function() {
                    console.log('unregister')
                    var push = window.plugins.pushNotification;
                    if (push) {
                        push.unregister(function() {
                            console.log('unregister success')
                        });
                    }
                }
            }
        });
        
        
        // ALL GCM notifications come through here. 
    function onNotification(e) {
        console.log('EVENT -> RECEIVED:' + e.event + '');
        alert('EVENT -> RECEIVED:' + e.event + '');
        switch (e.event)
        {
            case 'registered':
                if (e.regid.length > 0)
                {
                    console.log('REGISTERED with GCM Server -> REGID:' + e.regid + '');
                    //alert(e.regid);
                    
                    var elem = angular.element(document.querySelector('[ng-app]'));
                    var injector = elem.injector();
                    var myService = injector.get('PushProcessingService');
                    myService.registerID(e.regid);
                }
                break;

            case 'message':
                // if this flag is set, this notification happened while we were in the foreground. 
                if (e.foreground)
                {
                    //we're using the app when a message is received.
                    console.log('--INLINE NOTIFICATION--' + '');

                    // if the notification contains a soundname, play it.
                    //var my_media = new Media('/android_asset/www/'+e.soundname);
                    //my_media.play();
                    alert(e.payload.message);
                    var elem = angular.element(document.querySelector('[ng-app]'));
                    var injector = elem.injector();
                    var myService = injector.get('PushProcessingService');
                    myService.receivedToDo(e.payload.message);
                    
                    
                }
                else
                {
                    // otherwise we were launched because the user touched a notification in the notification tray.
                    if (e.coldstart){
                        console.log('--COLDSTART NOTIFICATION--' + JSON.stringify(e.payload));
                        alert(e.payload.message);
                            var elem = angular.element(document.querySelector('[ng-app]'));
                            var injector = elem.injector();
                            var myService = injector.get('PushProcessingService');
                            myService.receivedToDo(e.payload.message);
                    }
                    else{
                        console.log('--BACKGROUND NOTIFICATION--' + JSON.stringify(e.payload));
                            alert(e.payload.message);
                            var elem = angular.element(document.querySelector('[ng-app]'));
                            var injector = elem.injector();
                            var myService = injector.get('PushProcessingService');
                            myService.receivedToDo(e.payload.message);
                            // direct user here:
                    } 
                }

                console.log('MESSAGE -> MSG: ' + e.payload.message + '');
                console.log('MESSAGE: ' + JSON.stringify(e.payload));
                break;

            case 'error':
                console.log('ERROR -> MSG:' + e.msg + '');
                alert('ERROR -> MSG:' + e.msg + '');
                break;

            default:
                console.log('EVENT -> Unknown, an event was received and we do not know what it is');
                alert('EVENT -> Unknown, an event was received and we do not know what it is');
                break;
        }
    }

//app.factory('emailNoti',function(ConnectParse,$window,$q,$state,$ionicPopup,$localStorage){
//    $window.addEventListener('emailTask', function(e) {
//                        var task=e.detail.url;
//                                    if(task.length!=0){
//                                    var splitTask=task.split("?task=");
//                                        var todo=splitTask[1];
//                                        console.log(todo);
//                                    var confirm = $ionicPopup.confirm({
//                                        title: 'Received Task',
//                                        template: 'You have received a task '+todo.title+'.Do you want to add it ' ,
//                                        okText: "Yes",
//                                        cancelText: "No"
//                                    });
//                                    confirm.then(function(res) {
//                                        if(res){
//                                            if ($localStorage["Initializer"]){
//                                                var savetodo = $q.when(ConnectParse.save(todo));
//                                                    savetodo.then(
//                                                        function(result) {
//                                                            if (result) {
//                                                                console.log(result);
//                                                                $state.go("app.addTodo");
//                                                            }
//                                                        },
//                                                        function(error) {
//                                                            console.log(error);
//                                                        }
//                                                );
//                                            }
//                                            else{
//                                                var storedTodoTasks = [];
//                                                if ($localStorage['todoTasks']) {
//
//                                                    storedTodoTasks = $localStorage['todoTasks'];
//                                                    storedTodoTasks.push(todo);
//                                                    $localStorage['todoTasks'] = storedTodoTasks;
//                                                } else {
//                                                    storedTodoTasks.push(todo);
//                                                    $localStorage['todoTasks'] = storedTodoTasks;
//                                                }
//                                                console.log($localStorage['todoTasks']);
//                                            }
//                                        }
//                                    })
//                                }
//                     });
//});
//
//function handleOpenURL(url) {
//console.log("received url: " + url);
//var emailEvent=new CustomEvent('emailTask',{detail:{'url':url}});
//console.log(emailEvent);
//window.dispatchEvent(emailEvent);
////setTimeout(function(){
////     window.dispatchEvent(emailEvent);
////},0);
//
//}
