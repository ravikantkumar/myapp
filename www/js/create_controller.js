angular.module('create.controller', ["angular-datepicker"])
        .controller('CreateTodoCtrl', function($cordovaNetwork, $ionicHistory, $cordovaDatePicker, Alertuser, $rootScope, $scope, $state, $q, $localStorage, ConnectParse) {
            $scope.ChoosenDate = "Select a Date";
            //$scope.ChoosenTime="Select a Date";
//            $scope.datePickerOptions = {
//                format: 'dd-mm-yyyy', // ISO formatted date
//                onClose: function(e) {
//                    //alert(e);
//                    // do something when the picker closes   
//                }
//            };
//            $scope.TimePickerOptions = {
//                format: 'dd-mm-yyyy', // ISO formatted date
//                onClose: function(e) {
//                    //alert(e);
//                    // do something when the picker closes   
//                }
//            };
            $scope.now = {};
            $scope.now.date;
            $scope.now.time;
            $scope.showReminder = false;
            $scope.userPreTags = [];
            $scope.isshowrem = true;
            
            $scope.myDates = [
                {title: "Today"},
                {title: "Tomorrow"},
                {title: "Select a date.."}
            ];
            $scope.myTimes = [
                {title: "Morning", dtime: "9:00am"},
                {title: "Afternoon", dtime: "1:00pm"},
                {title: "Evening", dtime: "5:00pm"},
                {title: "Night", dtime: "8:00pm"},
                {title: "Select a time..", dtime: ""}
            ];
            $scope.mytime = $scope.myTimes[0];
            $scope.selectTime = function() {            // select the current time
                console.log("time");
                var timeflag = this.mytime.title;
                var date_options = {
                    date: new Date(),
                    mode: 'time', // or 'date'
                    minDate: new Date() - 10000,
                    allowOldDates: false,
                    allowFutureDates: true,
                    doneButtonLabel: 'Change',
                    doneButtonColor: '#F2F3F4',
                    cancelButtonLabel: 'Default',
                    cancelButtonColor: '#000000'
                };
                if (timeflag === "Morning") {
                    console.log("mor");
                    var d = new Date();
                    d.setHours(9);
                    d.setMinutes(0);
                    $scope.alarmTime = d.getTime();
                }
                if (timeflag === "Afternoon") {
                    console.log("aftre");
                    var d = new Date();
                    d.setHours(13);
                    d.setMinutes(0);
                    $scope.alarmTime = d.getTime();
                }
                if (timeflag === "Evening") {
                    console.log("Evening");
                    var d = new Date();
                    d.setHours(17);
                    d.setMinutes(0);
                    $scope.alarmTime = d.getTime();
                }
                if (timeflag === "Night") {
                    console.log("night");
                    var d = new Date();
                    d.setHours(20);
                    d.setMinutes(0);
                    $scope.alarmTime = d.getTime();
                }
                if (timeflag === "Select a time..") {
                    console.log("select time");
                    //if its mobile
                    if (window.cordova) {
                        if (window.cordova) {
                            $cordovaDatePicker.show(date_options).then(function(time) {
                                $scope.alarmTime = time.getTime();
                            });
                        }
                    }
                    //if its desktoop
                    else {
                        console.log("desktop");
                        var d = new Date();
                        $scope.alarmTime = d.getTime();
                        //$('.clockpicker').clockpicker('show');
                    }
                }
            };


            $scope.mydate = $scope.myDates[0];
            $scope.selectDate = function() {            //select the Date..
                var timeflag;
                var date_options = {
                    date: new Date(),
                    mode: 'date', // or 'time'
                    minDate: new Date() - 10000,
                    allowOldDates: false,
                    allowFutureDates: true,
                    doneButtonLabel: 'Change',
                    doneButtonColor: '#F2F3F4',
                    cancelButtonLabel: 'Default',
                    cancelButtonColor: '#000000'
                };
                timeflag = this.mydate.title;
                if (timeflag === "Today") {
                    $scope.todoAlarm = new Date().getTime();
                }
                if (timeflag === "Tomorrow") {
                    var d = new Date();
                    var tday = d.getDate();
                    d.setDate(tday + 1);
                    $scope.todoAlarm = d.getTime();
                }
                if (timeflag === "Select a date..") {
                    console.log("select");
                    //if its mobile
                    if (window.cordova) {
                        $cordovaDatePicker.show(date_options).then(function(date) {
                            $scope.todoAlarm = date.getTime();
                        });
                    }
                    //if its desktoop
                    else {
                        console.log("desktop");
                        $scope.todoAlarm = new Date().getTime();
                        //$('.clockpicker').clockpicker('show');
                    }
                }
            };

//            if (window.cordova && !$cordovaNetwork.isOnline()) {
//                $state.go('offline');
//            }
            $scope.defaultTime = function() {           // set time to default time...
                console.log("time adjusted");
                var d = new Date();
                var tdate = d.getDate();
                d.setDate(tdate);
                $scope.todoAlarm = d.getTime();
                var t = d.getHours();
                if (t >= 20 && t < 23) {
                    d.setDate(tdate + 1);
                    $scope.todoAlarm = d.getTime();
                    $scope.mydate = $scope.myDates[1];
                    $scope.mytime = $scope.myTimes[0];
                }
                else if (t >= 17 && t < 20) {
                    $scope.mytime = $scope.myTimes[3];
                    d.setHours(20);
                    d.setMinutes(0);
                    $scope.alarmTime = d.getTime();

                }
                else if (t >= 13 && t < 17) {
                    $scope.mytime = $scope.myTimes[2];
                    d.setHours(17);
                    d.setMinutes(0);
                    $scope.alarmTime = d.getTime();

                }
                else if (t >= 9 && t < 13) {
                    $scope.mytime = $scope.myTimes[1];
                    d.setHours(13);
                    d.setMinutes(0);
                    $scope.alarmTime = d.getTime();

                }
                else if (t > 0 && t < 9) {
                    $scope.mytime = $scope.myTimes[0];
                    d.setHours(9);
                    d.setMinutes(0);
                    $scope.alarmTime = d.getTime();
                }

                // end of curTime

            };
            $scope.$on('$ionicView.enter', function() {     // before enter to ionic view...

                //$scope.todoAlarm = $scope.mydate;
                $scope.defaultTime();
                $scope.curTime = new Date().getTime();

            }); // end of before view change
            var todostatus = {};
            todostatus.true = "item-completed";
            todostatus.false = "item-not-completed";
            // colour picker
            var color = [
                "#FFE6E6",
                "#FFB2B2",
                "#FFFFF0",
                "#F5E0EB",
                "#FAF0F5",
                "#E6FAF0",
                "#CCF5E0",
                "#E6F5FA",
                "#B2E0F0",
                "#FFE0B2",
                "#FFF5E6",
                "#FFF0FF",
                "#FFE6FF",
                "#E0EBEB",
                "#D1E0E0",
            ];
            $scope.addSelf = function(num) {
                var sum = 0;
                while (num > 0)
                {
                    sum += num % 10;
                    num = Math.floor(num / 10);
                }
                if (sum > 9)
                {
                    sum = $scope.addSelf(sum);
                }
                return sum;
            };

            $scope.showrem = function() {
                $scope.isshowrem = false;
            };

            $scope.mytask = {};
            // date picker

            $scope.noReminder = function() {
                $scope.showReminder = false;
                this.showReminder = false;
                $scope.todoAlarm = null;
                $scope.alarmTime = null;
            };
            $scope.reminderTi$locationme = {};
            var counter = 0;
            $scope.Date="Select a Date";
            $scope.Time="Select a Time";
            $scope.options = {
                format: 'yyyy-mm-dd', // ISO formatted date
                onClose: function(e) {
                    $state.go("app/createTodo");
                }
            }
            $scope.timeOptions = {
                format: 'HH-i', // ISO formatted date
                onClose: function(e) {
                    $state.go("app/createTodo");
                }
            }
            
            $scope.showDate = function(selection, e) {          // will shoe the Dates to get selected by user..
                if ($scope.reminderTime.isTimeDisplaying === false) {
                    $scope.reminderTime.isTimeDisplaying = "true";


                } else {
                    $scope.reminderTime.isTimeDisplaying = true;

                }
                //alert("click1");
                e.stopPropagation();
                console.log(selection);
                $scope.ChoosenDate = selection;
                console.log($scope.mydate.title);
                //adjust time
                var d = new Date();
                if (selection === "Today") {
                    $scope.todoAlarm = d.getTime();
                }
                if (selection === "Tomorrow") {
                    d.setDate(d.getDate() + 1);
                    $scope.todoAlarm = d.getTime();
                }

                // $scope.reminderTime.isDateDisplaying = !$scope.reminderTime.isDateDisplaying;
                if (selection === "Select a date..") {
                    $scope.pickDate();
                }

            };
            $scope.showTime = function(e, selection) {           // will shoe the Dates to get selected by user..
                //e.preventDefault();
                if ($scope.reminderTime.isTimeDisplaying === "true") {
                    $scope.reminderTime.isTimeDisplaying = false;

                } else {
                    $scope.reminderTime.isTimeDisplaying = false;

                }

                e.stopPropagation();
                //alert(selection);
                $scope.mytime.title = selection;
                var d = new Date();
                if (selection === "Morning") {
                    d.setHours(9);
                    d.setMinutes(0);
                    $scope.alarmTime = d.getTime();

                }
                if (selection === "Afternoon") {
                    d.setHours(13);
                    d.setMinutes(0);
                    d.setDate(d.getDate() + 1);
                    $scope.alarmTime = d.getTime();
                }
                if (selection === "Evening") {
                    d.setHours(17);
                    d.setMinutes(0);
                    $scope.alarmTime = d.getTime();
                }
                if (selection === "Night") {
                    d.setHours(20);
                    d.setMinutes(0);
                    d.setDate(d.getDate() + 1);
                    $scope.alarmTime = d.getTime();
                }

                if (selection === "Select a time..") {
                    $scope.pickTime();
                }
            };
            $scope.pickDate = function() {                              //Date Picker...
                if ($scope.reminderTime.isTimeDisplaying == true) {
                    $scope.reminderTime.isTimeDisplaying = false;
                }
                if ($scope.reminderTime.isDateDisplaying == true) {
                    $scope.reminderTime.isDateDisplaying = false;
                }
                var date_options = {
                    date: new Date(),
                    mode: 'date', // or 'time'
                    minDate: new Date() - 10000,
                    allowOldDates: false,
                    allowFutureDates: true,
                    doneButtonLabel: 'Change',
                    doneButtonColor: '#F2F3F4',
                    cancelButtonLabel: 'Default',
                    cancelButtonColor: '#000000'
                };
                $cordovaDatePicker.show(date_options).then(function(date) {
                    $scope.todoAlarm = date.getTime();
                });
            };
            $scope.reminderTime = {};
            $scope.reminderTime.isTimeDisplaying = false;
            $scope.reminderTime.isDateDisplaying = false;
            $scope.pickTime = function() {                               //time Picker...
                if ($scope.reminderTime.isTimeDisplaying == true) {
                    $scope.reminderTime.isTimeDisplaying = false;
                }
                if ($scope.reminderTime.isDateDisplaying == true) {
                    $scope.reminderTime.isDateDisplaying = false;
                }
                var time_options = {
                    date: new Date(),
                    mode: 'time', // or 'date'
                    minDate: new Date() - 10000,
                    allowOldDates: false,
                    allowFutureDates: true,
                    doneButtonLabel: 'Change',
                    doneButtonColor: '#F2F3F4',
                    cancelButtonLabel: 'Default',
                    cancelButtonColor: '#000000'
                };
                $cordovaDatePicker.show(time_options).then(function(time) {
                    $scope.alarmTime = time.getTime();
                });
            };

            $scope.addTodo = function() {                   // Create Todo.....

                if (!$scope.mytask.title || $scope.mytask.title === " " || $scope.mytask.title === "" || $scope.mytask.title.length === 0) {
                    if (window.cordova) {
                        Alertuser.alert("Please add the task");
                        return false;
                    }
                    else {
                        alert("Please add the task");
                        return false;
                    }
                }
                var val = $scope.mytask.title;
                console.log(new Date($scope.todoAlarm).getHours());
                var date_options = {
                    date: new Date(),
                    mode: 'date', // or 'time'
                    minDate: new Date() - 10000,
                    allowOldDates: false,
                    allowFutureDates: true,
                    doneButtonLabel: 'Change',
                    doneButtonColor: '#F2F3F4',
                    cancelButtonLabel: 'Default',
                    cancelButtonColor: '#000000'
                };
                if ($rootScope.ifdeviceReady && window.cordova) {   //  on Mobile
                    console.log("ready");
                    //$scope.todoAlarm = date.getTime();
                    //console.log($scope.todoAlarm);
                    if ($localStorage["Initializer"]) {         // On Mobile When user is Logged In

                        var checkTodoList = $q.when(ConnectParse.checkPosition(parseInt($localStorage["Initializer"])));
                        checkTodoList.then(
                                function(result) {
                                    if (result.length > 0) {
                                        newPos = result[0].get("position") + 1;
                                    }
                                    else {
                                        newPos = 0;
                                    }

                                    var todoObj = {};
                                    //poopulate todo item

                                    todoObj.title = val;
                                    if ($scope.todoAlarm) {
                                        todoObj.time = parseInt($scope.todoAlarm);
                                        console.log(new Date($scope.todoAlarm).getDate());
                                        console.log(new Date(todoObj.time).getDate());
                                    }
                                    if ($scope.alarmTime) {
                                        todoObj.alarmTime = $scope.alarmTime;
                                        console.log(new Date($scope.alarmTime).getHours());
                                        console.log(new Date(todoObj.alarmTime).getHours());
                                    }
                                    todoObj.parseStatus = true;
                                    todoObj.done = false;
                                    todoObj.userID = parseInt($localStorage["Initializer"]);
                                    todoObj.completed = todostatus[todoObj.done];
                                    todoObj.position = newPos;

                                    console.log(todoObj);
                                    Alertuser.saveAlert("Saved..");
                                    var storedTodoTasks = $localStorage['todoTasks'];
                                    storedTodoTasks.push(todoObj);
                                    $localStorage['todoTasks'] = storedTodoTasks;
                                    $ionicHistory.goBack();
                                    //$state.go('app.today');
                                    //set myToDoObject variable in cloud with values calling parse service
                                    var setToDoList = $q.when(ConnectParse.save(todoObj));
                                    setToDoList.then(
                                            function(result) {
                                                if (result) {
                                                    $scope.mytask.title = "";
                                                    console.log(result.title);
                                                    //schedule local notification fro saved todo
                                                    window.plugin.notification.local.add({
                                                        id: String(result.id), // A unique id of the notification
                                                        date: new Date($scope.alarmTime), // This expects a date object
                                                        message: result.title, // The message that is displayed
                                                        title: "Reminder", // The title of the message
                                                        autoCancel: true // Setting this flag and the notification is automatically cancelled when the user clicks it
                                                    });

                                                }
                                            },
                                            function(error) {
                                                console.log(error);
                                            }
                                    );

                                },
                                function(error) {
                                    console.log(error);
                                }
                        );
                        console.log("Y set" + $scope.todoAlarm);
                    } else {                                    // On Mobile When user is Logged Out

                        var newPos;
// ravi 
                        var result = [];
                        //result = $localStorage['todoTasks'];
                        if (!$localStorage['todoTasks']) {
                            newPos = 1;
                            console.log("Position :" + newPos);
                        }
                        else {
                            result = $localStorage['todoTasks'];
                            newPos = result.length + 1;
                            console.log("Position : " + newPos);
                        }

                        var todoObj = {};
                        //poopulate todo item
                        todoObj.Unique_Id = "";
                        todoObj.title = val;
                        if ($scope.todoAlarm) {
                            todoObj.time = parseInt($scope.todoAlarm);
                            console.log(new Date($scope.todoAlarm).getDate());
                            console.log(new Date(todoObj.time).getDate());
                        }
                        if ($scope.alarmTime) {
                            todoObj.alarmTime = $scope.alarmTime;
                            console.log(new Date($scope.alarmTime).getHours());
                            console.log(new Date(todoObj.alarmTime).getHours());
                        }
                        todoObj.parseStatus = true;
                        todoObj.done = false;
                        // todoObj.userID = parseInt($localStorage["Initializer"]);
                        todoObj.completed = todostatus[todoObj.done];
                        todoObj.position = newPos;

                        console.log(todoObj);

                        var storedTodoTasks = [];

                        if ($localStorage['todoTasks']) {

                            storedTodoTasks = $localStorage['todoTasks'];
                            storedTodoTasks.push(todoObj);
                            $localStorage['todoTasks'] = storedTodoTasks;
                        } else {
                            storedTodoTasks.push(todoObj);
                            $localStorage['todoTasks'] = storedTodoTasks;
                        }
                        console.log($localStorage['todoTasks']);
                        Alertuser.saveAlert("Saved..");

                        $ionicHistory.goBack();
                        //$state.go('app.today');

                        console.log("Y set" + $scope.todoAlarm);
                    }
                } else {                                        // On Desktop
                    if ($localStorage["Initializer"]) {         // On Desktop when user is Logged in 

                        var checkTodoList = $q.when(ConnectParse.checkPosition(parseInt($localStorage["Initializer"])));
                        checkTodoList.then(
                                function(result) {
                                    if (result.length > 0) {
                                        newPos = result[0].get("position") + 1;
                                    }
                                    else {
                                        newPos = 0;
                                    }

                                    var todoObj = {};
                                    //poopulate todo item

                                    todoObj.title = val;
                                    if ($scope.todoAlarm) {
                                        todoObj.time = parseInt($scope.todoAlarm);
                                        console.log(new Date($scope.todoAlarm).getDate());
                                        console.log(new Date(todoObj.time).getDate());
                                    }
                                    if ($scope.alarmTime) {
                                        todoObj.alarmTime = $scope.alarmTime;
                                        console.log(new Date($scope.alarmTime).getHours());
                                        console.log(new Date(todoObj.alarmTime).getHours());
                                    }
                                    todoObj.parseStatus = true;
                                    todoObj.done = false;
                                    todoObj.userID = parseInt($localStorage["Initializer"]);
                                    todoObj.completed = todostatus[todoObj.done];
                                    todoObj.position = newPos;

                                    console.log(todoObj);
                                    Alertuser.saveAlert("Saved..");
                                    var storedTodoTasks = $localStorage['todoTasks'];
                                    storedTodoTasks.push(todoObj);
                                    $localStorage['todoTasks'] = storedTodoTasks;
                                    console.log("here");
                                    $ionicHistory.goBack();
                                    //$state.go('app.today');
                                    //set myToDoObject variable in cloud with values calling parse service
                                    var setToDoList = $q.when(ConnectParse.save(todoObj));
                                    setToDoList.then(
                                            function(result) {
                                                if (result) {
                                                    // $scope.id=result.Unique_Id;
                                                    $scope.mytask.title = "";
                                                    console.log(result.title);
                                                    //schedule local notification fro saved todo
                                                    window.plugin.notification.local.add({
                                                        id: String(result.id), // A unique id of the notification
                                                        date: new Date($scope.alarmTime), // This expects a date object
                                                        message: result.title, // The message that is displayed
                                                        title: "Reminder", // The title of the message
                                                        autoCancel: true // Setting this flag and the notification is automatically cancelled when the user clicks it
                                                    });

                                                }
                                            },
                                            function(error) {
                                                console.log(error);
                                            }
                                    );

                                },
                                function(error) {
                                    console.log(error);
                                }
                        );
                        console.log("Y set" + $scope.todoAlarm);
                    } else {                                        // On Desktop when user is Logged Out

                        var newPos;
// ravi 
                        var result = [];
                        result = $localStorage['todoTasks'];
                        if (!result) {
                            newPos = 1;
                        }
                        else {
                            newPos = result.length + 1;
                        }

                        var todoObj = {};
                        //poopulate todo item
                        todoObj.Unique_Id = "";
                        todoObj.title = val;
                        if ($scope.todoAlarm) {
                            todoObj.time = parseInt($scope.todoAlarm);
                            console.log(new Date($scope.todoAlarm).getDate());
                            console.log(new Date(todoObj.time).getDate());
                        }
                        if ($scope.alarmTime) {
                            todoObj.alarmTime = $scope.alarmTime;
                            console.log(new Date($scope.alarmTime).getHours());
                            console.log(new Date(todoObj.alarmTime).getHours());
                        }
                        todoObj.parseStatus = true;
                        todoObj.done = false;
                        //todoObj.userID = parseInt($localStorage["Initializer"]);
                        todoObj.completed = todostatus[todoObj.done];
                        todoObj.position = newPos;

                        console.log(todoObj);

                        var storedTodoTasks = [];

                        if ($localStorage['todoTasks']) {
                            storedTodoTasks = $localStorage['todoTasks'];
                            storedTodoTasks.push(todoObj);
                            $localStorage['todoTasks'] = storedTodoTasks;
                        } else {
                            storedTodoTasks.push(todoObj);
                            $localStorage['todoTasks'] = storedTodoTasks;
                        }

                        Alertuser.saveAlert("desktop--logout--Saved..");

                        $ionicHistory.goBack();
                        //$state.go('app.today');

                        console.log("Y set" + $scope.todoAlarm);
                    }
                }


            };
        });

