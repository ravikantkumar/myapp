
angular.module('starter', ['ionic', 'ngCordova', 'offline.controller', 'create.controller', 'fb.controller', 'app.controller', 'starter.controllers', 'starter.services', 'starter.factory','starter.directives', 'starter.filters','ngStorage','angular-datepicker'])
        .run(['$ionicPlatform','$rootScope','$state','$cordovaNetwork','$location','$localStorage','PushProcessingService',function($ionicPlatform, $rootScope, $state, $cordovaNetwork, $location, $localStorage,PushProcessingService) {
            
                    $ionicPlatform.ready(function() {
                // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
                // for form inputs)
                if (window.cordova && $cordovaNetwork.isOnline()) {
                    $rootScope.ifdeviceReady = true;
                    $rootScope.channel="";
                    if (window.cordova && window.cordova.plugins.Keyboard) {
                        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                    }
                    if (window.StatusBar) {
                        // org.apache.cordova.statusbar required
                        StatusBar.styleDefault();
                        StatusBar.backgroundColorByHexString("#039be5");
                    }
//                    facebookConnectPlugin.getLoginStatus(function(response){
//                        if(response.status === 'connected'){
//                            var uid = response.authResponse.userID;
//                            alert(uid);
//                        }
//                    });
                            // Initialize Push Notification Service
                            PushProcessingService.initialize();
                   

//                    else if($location.path("/login") && $localStorage['Initializer']) {
//                         $state.go("app.addTodo");
//                        //$state.go("login");
//                    }

//                    ParsePushPlugin.register({
//                        appId: "F8TOhK8zoN69mY0OydaZBgVOcFT4xAxlLYegGFX2", clientKey: "G1C9h3fBpsj1xdtuwNJmJODoWxTjxl9hapSblF72", eventKey: "myEventKey"}, //will trigger receivePN[pnObj.myEventKey]
//                    function() {
//                        alert('successfully registered device!');
//                    }, function(e) {
//                        alert('error registering device: ' + e);
//                    });
//
//                    ParsePushPlugin.getInstallationId(function(id) {
//                        alert(id);
//                    }, function(e) {
//                        alert('error');
//                    });
//                  
//                    ParsePushPlugin.subscribe("ch_368896649978284", function (msg) {
//                        alert('OK');
//                        defer.resolve(msg);
//                    }, function(e) {
//                        defer.reject(e);
//                    });
//                    
////                    ParsePushPlugin.unsubscribe("ch_368896649978284", function (msg) {
////                        alert('OK');
////                        defer.resolve(msg);
////                    }, function(e) {
////                        defer.reject(e);
////                    });
//
//                    ParsePushPlugin.on('receivePN', function(pn) {
//                        alert('yo i got this push notification:' + JSON.stringify(pn));
//                    });

                 
                }
            });
        }])
        .config(['$compileProvider', function($compileProvider) {
                $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|ftp|file|blob|content):|data:image\//);
            }])

        .config(function($stateProvider, $urlRouterProvider) {
            $stateProvider
                    .state('login', {
                        url: "/login",
                        cache: false,
                        templateUrl: "templates/login.html",
                        controller: 'FbCtrl'
                    })
                    .state('fb', {
                        url: "/fb",
                        cache: false,
                        templateUrl: "templates/fb_login.html",
                        controller: 'LoginCtrl'
                    })
                    .state('offline', {
                        url: "/offline",
                        cache: false,
                        templateUrl: "templates/offline.html",
                        controller: 'OfflineCtrl'
                    })
                    .state('app', {
                        url: "/app",
                        abstract: true,
                        templateUrl: "templates/menu.html",
                        controller: 'AppCtrl'
                    })
//                    .state('app.category', {
//                        url: "/category",
//                        cache: false,
//                        views: {
//                            'menuContent': {
//                                templateUrl: "templates/category.html",
//                                controller: 'AddTodoCtrl'
//                            }
//                        }
//                    })

                    .state('app.createTodo', {
                        url: "/createTodo",
                        cache: false,
                        views: {
                            'menuContent': {
                                templateUrl: "templates/createTodo.html",
                                controller: 'CreateTodoCtrl'
                            }
                        }
                    })
//                    .state('app.fbShareTodo', {
//                        url: "/fbShareTodo",
//                        cache: false,
//                        views: {
//                            'menuContent': {
//                                templateUrl: "templates/fbShareTodo.html",
//                                controller: 'fbShareTodoCtrl'
//                            }
//                        }
//                    })
                    .state('app.complete', {
                        url: "/complete_todo",
                        cache: false,
                        views: {
                            'menuContent': {
                                templateUrl: "templates/completed.html",
                                controller: 'AddTodoCtrl'
                            }
                        }
                    })
                    .state('app.today', {
                        url: "/today",
                        cache: false,
                        views: {
                            'menuContent': {
                                templateUrl: "templates/today.html",
                                controller: 'AddTodoCtrl'
                            }
                        }
                    })
                    .state('app.trash', {
                        url: "/trash",
                        cache: false,
                        views: {
                            'menuContent': {
                                templateUrl: "templates/trash.html",
                                controller: 'AddTodoCtrl'
                            }
                        }
                    })
//                    .state('app.tasks', {
//                        url: "/tasks",
//                        cache: false,
//                        views: {
//                            'menuContent': {
//                                templateUrl: "templates/tasks.html",
//                                controller: 'AddTodoCtrl'
//                            }
//                        }
//                    })
                    .state('app.addTodo', {
                        url: "/add_todo",
                        cache: false,
                        views: {
                            'menuContent': {
                                templateUrl: "templates/add_todo.html",
                                controller: 'AddTodoCtrl'
                            }
                        }
                    });
            // if none of the above states are matched, use this as the fallback
            //$urlRouterProvider.otherwise('/app/add_todo');
            $urlRouterProvider.otherwise('/app/add_todo');
        });
 function handleOpenURL(url) {
console.log("received url: " + url);

};